/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.Pilot;

/**
 * Class contains all DB queries.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class DBQueryImpl implements DBQuery {

    /**
     * logger for DBQueryImpl class
     */
    private static final Logger LOGGER = Logger.getLogger(DBQueryImpl.class);
    
    /**
     * db column name for longitude
     */
    private static final String DB_LONGITUDE = "Longitude";
    
    /**
     * db column name for latitude
     */
    private static final String DB_LATITUDE = "Latitude";
    
    /**
     * db column name for airport id
     */
    private static final String DB_AIRPORT_PID = "airport.PID";

    private Connection connection;

    /**
     * Creates a new DBQuery class and establish a connection to the db over the
     * JDBCConnector Class
     */
    public DBQueryImpl(Connection connection) {
        this.connection = connection;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ch.bfh.smartflyer.persistence.DBQuery#getPilotById(int)
     */
    @Override
    public Pilot getPilotById(int id) {

        String query = "SELECT * FROM `pilot`, `airport` WHERE pilot.PID = ? AND pilot.FavouriteAirportID = airport.PID";

        try (PreparedStatement pstmt = connection.prepareStatement(query)) {

            pstmt.setInt(1, id); // Compliant; PreparedStatements escape their
                                 // inputs.
            ResultSet result = pstmt.executeQuery();

            if (result == null || !result.next()) {
                LOGGER.info("No Pilot with the id " + id
                        + " was found in the database");
            } else {

                Airport airport = new Airport(result.getInt(DB_AIRPORT_PID),
                        result.getString("City"), result.getString("IATA"),
                        result.getString("ICAO"), result.getDouble(DB_LATITUDE),
                        result.getDouble(DB_LONGITUDE));

                return new Pilot(result.getInt("pilot.PID"),
                        result.getString("Firstname"),
                        result.getString("Lastname"),
                        result.getDouble("Weight"), airport);
            }
        } catch (SQLException e) {
            LOGGER.error("Could not Execute Query \"getPilotById\"", e);
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ch.bfh.smartflyer.persistence.DBQuery#addFlight(int, int, int)
     */
    @Override
    public void insertFlight(int pilotID, int fromAirportID, int toAirportID,
            Date date) {

        String query = "INSERT INTO `flight` (`PilotID`, `FromAirportID`, `ToAirportID`, `Date`) "
                + "VALUES (?,?,?,?)";

        try (PreparedStatement pstmt = connection.prepareStatement(query)) {

            pstmt.setInt(1, pilotID);
            pstmt.setInt(2, fromAirportID);
            pstmt.setInt(3, toAirportID);
            pstmt.setTimestamp(4, new Timestamp(date.getTime()));
            pstmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error("Could not Execute Query \"addFlight\"", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ch.bfh.smartflyer.persistence.DBQuery#insertPilot(ch.bfh.smartflyer.domain
     * .Pilot)
     */
    @Override
    public void insertPilot(Pilot p) {
        String query = "INSERT INTO `pilot` (`Firstname`, `Lastname`, `Weight`, `FavouriteAirportID`) "
                + "VALUES (?,?,?,?)";

        try (PreparedStatement pstmt = connection.prepareStatement(query)) {

            pstmt.setString(1, p.getFirstname());
            pstmt.setString(2, p.getLastname());
            pstmt.setDouble(3, p.getWeight());
            pstmt.setInt(4, p.getFavouriteAirport().getId());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error("Could not Execute Query \"insertPilot\"", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see ch.bfh.smartflyer.persistence.DBQuery#getAllAirports()
     */
    @Override
    public List<Airport> getAllAirports() {

        List<Airport> airports = new ArrayList<Airport>();
        String query = "SELECT * FROM `airport`";

        try (PreparedStatement pstmt = connection.prepareStatement(query)) {

            ResultSet result = pstmt.executeQuery();

            if (result == null || !result.next()) {
                LOGGER.info("No Airports were found in database. Please ensure, "
                        + "that you inserted demo airport data into database.");
            } else {
                do {
                    Airport airport = new Airport(result.getInt(DB_AIRPORT_PID),
                            result.getString("City"), result.getString("IATA"),
                            result.getString("ICAO"),
                            result.getDouble(DB_LATITUDE),
                            result.getDouble(DB_LONGITUDE));
                    airports.add(airport);

                } while (result.next());
            }
        } catch (SQLException e) {
            LOGGER.error("Could not Execute Query \"getAllAirports\"", e);
        }

        return airports;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ch.bfh.smartflyer.persistence.DBQuery#getAllPilots()
     */
    @Override
    public List<Pilot> getAllPilots() {

        List<Pilot> pilots = new ArrayList<Pilot>();

        String query = "SELECT * FROM `pilot`, `airport` WHERE pilot.FavouriteAirportID = airport.PID";

        try (PreparedStatement pstmt = connection.prepareStatement(query)) {

            ResultSet result = pstmt.executeQuery();

            if (result == null || !result.next()) {
                LOGGER.info("No Pilots were found in database. Please ensure, "
                        + "that you inserted demo pilot data into database.");
            } else {
                do {
                    Airport airport = new Airport(result.getInt(DB_AIRPORT_PID),
                            result.getString("City"), result.getString("IATA"),
                            result.getString("ICAO"),
                            result.getDouble(DB_LATITUDE),
                            result.getDouble(DB_LONGITUDE));

                    Pilot pilot = new Pilot(result.getInt("pilot.PID"),
                            result.getString("Firstname"),
                            result.getString("Lastname"),
                            result.getDouble("Weight"), airport);

                    pilots.add(pilot);
                } while (result.next());

            }
        } catch (SQLException e) {
            LOGGER.error("Could not Execute Query \"getAllPilots\"", e);
        }
        return pilots;
    }
}
