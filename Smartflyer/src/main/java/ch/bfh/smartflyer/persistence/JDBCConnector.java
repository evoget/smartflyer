/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * JDBC HelperClass. Loads the db properties and connects to the mysql DB.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class JDBCConnector {

    private static final Logger LOGGER = Logger.getLogger(JDBCConnector.class);

    private Connection connection;

    /**
     * Loads the db username and password from the properties file
     * 
     * @return the db properties
     */
    private Properties loadProperties() {
        Properties prop = new Properties();

        InputStream input = getClass().getClassLoader().getResourceAsStream(
                "db.properties");
        try {
            prop.load(input);
        } catch (IOException e) {
            LOGGER.error("Could not read db properties from file", e);
        }

        return prop;
    }

    /**
     * Attempts to establish a connection to the given database URL.
     * 
     * @return the connection to the db url if successful, null otherwise
     * @throws SQLException
     *             Problem with the Database Server
     * @throws ClassNotFoundException
     */
    private Connection connect() throws SQLException, ClassNotFoundException {

        Connection conn = null;

        Properties p = loadProperties();

        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost/smartflyer",
                p.getProperty("mysql.user"), p.getProperty("mysql.pwd"));

        return conn;
    }

    /**
     * Returns the database connection.
     * 
     * @return the database connection
     * @throws SQLException
     *             Problem with the Database Server
     * @throws ClassNotFoundException
     */
    public Connection getConnection() throws ClassNotFoundException,
            SQLException {

        if (connection == null) {
            return connect();
        }
        return connection;
    }
}
