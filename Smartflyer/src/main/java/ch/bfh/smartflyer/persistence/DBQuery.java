/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.persistence;

import java.util.Date;
import java.util.List;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.Pilot;

/**
 * Interface containing all DB querys for the pilot database
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public interface DBQuery {

    /**
     * Searches a pilot by the id
     * 
     * @param id
     *            the id of the pilot
     * @return the pilot with this id, null otherwise
     */
    public Pilot getPilotById(int id);

    /**
     * Insert a new pilot into database
     * 
     * @param p
     *            the new pilot to insert
     */
    public void insertPilot(Pilot p);

    /**
     * Inserts a new flight into database
     * 
     * @param pilotID
     *            the id of the pilot
     * @param fromAirportID
     *            the id of the starting airport
     * @param toAirportID
     *            the id of the landing airport
     * @param date
     *            the date of the flight
     */
    public void insertFlight(int pilotID, int fromAirportID, int toAirportID,
            Date date);

    /**
     * Returns a List with all Airports found in database
     * 
     * @return the list of airports
     */
    public List<Airport> getAllAirports();

    /**
     * Returns a List with all Pilots found in database
     * 
     * @return the list of pilots
     */
    public List<Pilot> getAllPilots();
}
