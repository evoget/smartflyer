/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Class provides method to calculate the distance between two Airports
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class AirportDistanceCalculator {

    private static final double EARTH_RADIUS = 6371000.0; // meters

    /**
     * private constructor
     */
    private AirportDistanceCalculator() {
        // private constructor
    }

    /**
     * Calculates the distance between two geo points
     * 
     * @param lat1
     *            latitude of starting point
     * @param lng1
     *            longitude of starting point
     * @param lat2
     *            latitude of ending point
     * @param lng2
     *            longitude of ending point
     * @return the distance between the two points in meters
     */
    public static double calculateDistance(double lat1, double lng1,
            double lat2, double lng2) {

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }
}
