/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Not enough Petrol Exception
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class NotEnoughPetrolException extends SmartflyerException {
    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8604952460259639582L;

    public NotEnoughPetrolException() {
        super();
    }

    @Override
    public String printFailure() {
        return "Not enough Petrol";
    }
}
