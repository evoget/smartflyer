/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Not enough battery reserve Exception
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class BatteryCapacityUnterAuxiliaryException extends SmartflyerException {
    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1624125761865647220L;

    public BatteryCapacityUnterAuxiliaryException() {
        super();
    }

    @Override
    public String printFailure() {
        return "Not enough battery reserve";
    }
}
