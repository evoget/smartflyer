/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Smartflyer Exception
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public abstract class SmartflyerException extends Exception {
    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1927506923317789208L;

    public SmartflyerException() {
        super();
    }

    public abstract String printFailure();
}
