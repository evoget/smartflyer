/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Flight too short Exception
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class FlightTooShortException extends SmartflyerException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 669953929046872384L;

    public FlightTooShortException() {
        super();
    }

    @Override
    public String printFailure() {
        return "Flight too short for this altitude";
    }
}
