/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

/**
 * Not enough Battery Exception
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class NotEnoughBatteryException extends SmartflyerException {
    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8236403729733916878L;

    public NotEnoughBatteryException() {
        super();
    }

    @Override
    public String printFailure() {
        return "Not enough battery";
    }
}
