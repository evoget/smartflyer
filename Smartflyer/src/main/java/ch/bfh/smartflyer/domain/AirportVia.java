/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * AirportVia is used to add a via destination to the flight. It contains an
 * Airport and also the length of stay and if there is a power source.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class AirportVia {

    /**
     * the airport of this via
     */
    private Airport airport;

    /**
     * the length of stay at the via airport
     */
    private double lengthOfStay;

    /**
     * indicated, if there is a power source at the airport
     */
    private boolean powerSource;

    /**
     * Constructor to create a new airport via
     * 
     * @param airport
     *            the airport of the via
     * @param lengthOfStay
     *            the length of stay at this via airport
     * @param powerSource
     *            indicates, if there is a power source a the airport
     */
    public AirportVia(Airport airport, double lengthOfStay, boolean powerSource) {
        super();
        this.airport = airport;
        this.lengthOfStay = lengthOfStay;
        this.powerSource = powerSource;
    }

    // Getter and Setter

    public Airport getAirport() {
        return airport;
    }

    public double getLengthOfStay() {
        return lengthOfStay;
    }

    public boolean getPowerSource() {
        return powerSource;
    }

    @Override
    public String toString() {
        return airport.getName() + " " + airport.getIata() + "/"
                + airport.getIcao();
    }
}
