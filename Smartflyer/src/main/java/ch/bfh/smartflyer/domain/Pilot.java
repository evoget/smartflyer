/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * Pilot domain class. Represents the pilot with last- and firstname.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Pilot {

    /**
     * id of the pilot
     */
    private int id;

    /**
     * firstname of the pilot
     */
    private String firstname;

    /**
     * lastname of the pilot
     */
    private String lastname;

    /**
     * weight of the pilot
     */
    private double weight;

    /**
     * favored airport of the pilot
     */
    private Airport favouriteAirport;

    /**
     * Create a new pilot with a name, his weight and the favored airport.
     * 
     * @param firstname
     * @param lastname
     * @param weight
     * @param favouriteAirport
     */
    public Pilot(int id, String firstname, String lastname, double weight,
            Airport favouriteAirport) {
        super();
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.weight = weight;
        this.favouriteAirport = favouriteAirport;
    }

    // getter and setters
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Airport getFavouriteAirport() {
        return favouriteAirport;
    }

    public void setFavouriteAirport(Airport favouriteAirport) {
        this.favouriteAirport = favouriteAirport;
    }

    @Override
    public String toString() {
        return lastname + " " + firstname;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((firstname == null) ? 0 : firstname.hashCode());
        result = prime * result + id;
        result = prime * result
                + ((lastname == null) ? 0 : lastname.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pilot other = (Pilot) obj;
        if (firstname == null) {
            if (other.firstname != null)
                return false;
        } else if (!firstname.equals(other.firstname))
            return false;
        if (id != other.id)
            return false;
        if (lastname == null) {
            if (other.lastname != null)
                return false;
        } else if (!lastname.equals(other.lastname))
            return false;
        return true;
    }
}
