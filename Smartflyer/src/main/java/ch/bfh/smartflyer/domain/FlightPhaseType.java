/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * The different types of the flightphases
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public enum FlightPhaseType {

    /**
     * start phase
     */
    TKOF("Start"),

    /**
     * climb phase
     */
    CLB("Climb"),

    /**
     * cruise phase (horizontal flight)
     */
    CRS("Cruise"),

    /**
     * descend phase
     */
    DES("Descend"),

    /**
     * intermediate landing phase
     */
    AIRPORT_STOP("Intermediate landing"),

    /**
     * intermediate landing phase with power source
     */
    AIRPORT_STOP_POWERSOURCE("Intermediate landing with power source");

    /**
     * the name of the flightphase
     */
    private final String fligthPhaseName;

    /**
     * creates a new FlightphaseType
     * 
     * @param name
     *            the name of the flightphase
     */
    private FlightPhaseType(String name) {
        this.fligthPhaseName = name;
    }

    public String getFligthPhaseName() {
        return fligthPhaseName;
    }

}
