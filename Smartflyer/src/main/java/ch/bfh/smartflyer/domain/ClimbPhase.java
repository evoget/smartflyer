/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A climbphase for the smartflyer. Represents the phase, when the plane is
 * climbing.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class ClimbPhase extends Flightphase {

    /**
     * the vertical distance (height) the plane covers in this phase
     */
    private double verticalDistance;

    /**
     * the rate of climb (meters per second)
     */
    private static final double RATE_OF_CLIMB = 2.54;

    /**
     * the true air speed (meters per second)
     */
    private static final double TRUE_AIR_SPEED = 41.16;

    /**
     * performance at the propeller (in kilowatt)
     */
    private static final double KW = 80;

    /**
     * Creates a new ClimbPhase with a vertical distance
     * 
     * @param verticalDistance
     *            the height the plane has to reach in this phase
     */
    public ClimbPhase(double verticalDistance) {
        super(KW, FlightPhaseType.CLB);
        this.verticalDistance = verticalDistance;
    }

    @Override
    public double getFlightTime() {
        return verticalDistance / RATE_OF_CLIMB;
    }

    @Override
    public double getHorizontalDistance() {
        return getFlightTime() * TRUE_AIR_SPEED;
    }

    @Override
    public double getRateOfClimb() {
        return RATE_OF_CLIMB;
    }

    @Override
    public double getSpeed() {
        return TRUE_AIR_SPEED;
    }
}
