/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Intermediate Landing Phase for the smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class IntermediateLanding extends Flightphase {

    /**
     * the length of stay (in minutes)
     */
    private double lengthOfStay;

    /**
     * Create new Intermediate Landing Phase
     * 
     * @param lengthOfStay
     *            the duration of the stay
     */
    public IntermediateLanding(double lengthOfStay) {
        super(0, FlightPhaseType.AIRPORT_STOP);

        this.lengthOfStay = lengthOfStay;
    }

    @Override
    public double getFlightTime() {
        return lengthOfStay;
    }

    @Override
    public double getHorizontalDistance() {
        return 0;
    }

    @Override
    public double getRateOfClimb() {
        return 0;
    }

    @Override
    public double getSpeed() {
        return 0;
    }

}
