/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Tank for the smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Tank {

    /**
     * the maximum capacity of the tank
     */
    private double maxCapacity;
    
    /**
     * the current capacity of the tank
     */
    private double currentCapacity;

    /**
     * Creates a new Tank
     * 
     * @param maxCapacity
     *            the max capacity of this tank
     * @param currentCapacity
     *            the current capacity of this tank
     */
    public Tank(double maxCapacity, double currentCapacity) {
        super();
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
    }

    /**
     * @return the currentCapacity
     */
    public double getCurrentCapacity() {
        return currentCapacity;
    }

    /**
     * @param currentCapacity
     *            the currentCapacity to set
     */
    public void setCurrentCapacity(double currentCapacity) {
        this.currentCapacity = currentCapacity;
    }

    /**
     * @return the maxCapacity
     */
    public double getMaxCapacity() {
        return maxCapacity;
    }

}
