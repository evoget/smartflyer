/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Electromotor for s smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Electromotor {
    
    /**
     * the maximum power of the electromotor
     */
    private double maxPower;
    
    /**
     * the current power of the electromotor
     */
    private double currentPower;

    /**
     * Creates a Electromotor
     * 
     * @param maxPower
     *            max power of the motor
     * @param currentPower
     *            the current power of the motor
     */
    public Electromotor(double maxPower, double currentPower) {
        super();
        this.maxPower = maxPower;
        this.currentPower = currentPower;
    }

    /**
     * Creates a Electromotor without power values
     */
    public Electromotor() {
        super();
    }

    /**
     * @return the power
     */
    public double getPower() {
        return maxPower;
    }

    /**
     * @return the currentPower
     */
    public double getCurrentPower() {
        return currentPower;
    }

    /**
     * @param currentPower
     *            the currentPower to set
     */
    public void setCurrentPower(double currentPower) {
        this.currentPower = currentPower;
    }

    /**
     * @return the maxPower
     */
    public double getMaxPower() {
        return maxPower;
    }
}
