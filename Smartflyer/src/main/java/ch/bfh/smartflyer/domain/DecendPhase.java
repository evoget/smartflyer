/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Decend Phase for the smartflyer. The descent of the plane.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class DecendPhase extends Flightphase {

    /**
     * performance at the propeller (in kilowatt)
     */
    private static final double KW = 10;

    /**
     * the rate of climb (meters per second)
     */
    private static final double RATE_OF_CLIMB = 2.54;

    /**
     * the height (in meters), at which the plane descend faster
     */
    private static final double HIGH_SPEED_ALTITUDE = 6000 / 3.2808; // faster
                                                                     // over
    // 6000ft
    
    /**
     * the true air speed (low) (meters per second)
     */
    private static final double LOW_SPEED = 51.44;
    
    /**
     * the true air speed (high) (meters per second)
     */
    private static final double HIGH_SPEED = 56.59;
    
    /**
     * the altitude (in meters)
     */
    private double altitude;
    
    /**
     * the true air speed (meters per second)
     */
    private double trueAirSpeed;

    /**
     * Creates a new descend phase
     * 
     * @param altitude
     *            the altitude
     */
    public DecendPhase(double altitude) {
        super(KW, FlightPhaseType.DES);
        this.altitude = altitude;

        if (this.altitude < HIGH_SPEED_ALTITUDE) {
            this.trueAirSpeed = LOW_SPEED;
        } else {
            this.trueAirSpeed = HIGH_SPEED;
        }
    }

    @Override
    public double getFlightTime() {
        return altitude / RATE_OF_CLIMB;
    }

    @Override
    public double getHorizontalDistance() {
        return getFlightTime() * trueAirSpeed;
    }

    @Override
    public double getRateOfClimb() {
        return -RATE_OF_CLIMB;
    }

    @Override
    public double getSpeed() {
        return trueAirSpeed;
    }
}
