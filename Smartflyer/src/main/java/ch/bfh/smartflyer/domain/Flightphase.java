/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * Flightphase domain class. A flight contains different flight phases. Each
 * flight phase has the same attributes but different values.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public abstract class Flightphase {

    /**
     * performance at the propeller (in kilowatt)
     */
    private double kW;

    /**
     * type of the flight phase
     */
    private FlightPhaseType type;

    /**
     * Creates a new flightphase with a performance and the flightphase type
     * 
     * @param kW
     *            the performance at the propeller
     * @param type
     *            the type of the flightphase
     */
    public Flightphase(double kW, FlightPhaseType type) {
        this.kW = kW;
        this.type = type;
    }

    /**
     * @return the total flight time (minutes) of this flightphase
     */
    public abstract double getFlightTime();

    /**
     * @return the total traveled horizontal distance (meters) of this
     *         flightphase
     */
    public abstract double getHorizontalDistance();

    /**
     * @return the rate of climb (meters per second) of this flightphase
     */
    public abstract double getRateOfClimb();

    /**
     * @return the true air speed (meters per second) of this flightphase
     */
    public abstract double getSpeed();

    /**
     * @return the power consumption of this flightphase
     */
    public double getPowerConsumption() {
        return (getFlightTime() * kW) / 3600;
    }

    /**
     * @return the current power consumption
     */
    public double getCurrentPowerConsumption() {
        return kW;
    }

    /**
     * @return the type of this flightphase
     */
    public FlightPhaseType getFlightPhaseType() {
        return type;
    }

}
