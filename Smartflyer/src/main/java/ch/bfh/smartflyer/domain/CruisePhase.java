/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * Cruise Phase for the smartflyer. Represents the horizontal flight of the
 * plane.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class CruisePhase extends Flightphase {

    /**
     * performance at the propeller (in kilowatt)
     */
    public static final double KW = 50;

    /**
     * horizontal distance to travel (in meters)
     */
    private double horizontalDistance;

    /**
     * the true air speed (meters per second)
     */
    private double trueAirSpeed;

    /**
     * the altitude (in meters)
     */
    private double altitude;

    /**
     * Creates a new Cruise Phase
     * 
     * @param horizontalDistance
     *            horizontal distance
     * @param altitude
     *            the altitude
     */
    public CruisePhase(double horizontalDistance, double altitude) {
        super(KW, FlightPhaseType.CRS);
        this.altitude = altitude;
        this.horizontalDistance = horizontalDistance;

        if (this.altitude < 1828) {
            this.trueAirSpeed = 61.73;
        } else if (this.altitude < 2743) {
            this.trueAirSpeed = 66.88;
        } else {
            this.trueAirSpeed = 72.02;
        }
    }

    @Override
    public double getFlightTime() {
        return horizontalDistance / trueAirSpeed;
    }

    @Override
    public double getHorizontalDistance() {
        return horizontalDistance;
    }

    public double getAltitude() {
        return altitude;
    }

    @Override
    public double getRateOfClimb() {
        return 0;
    }

    @Override
    public double getSpeed() {
        return trueAirSpeed;
    }
}
