/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A battery for a smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Battery {

    /**
     * the maximum capacity of the battery
     */
    private double maxCapacity;

    /**
     * the current capacity of the battery
     */
    private double currentCapacity;

    /**
     * Creates a new battery with a maximum- and the current battery capacity
     * 
     * @param maxCapacity
     *            maximum capacity of the battery
     * @param currentCapacity
     *            current capacity of the battery
     */
    public Battery(double maxCapacity, double currentCapacity) {
        super();
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
    }

    /**
     * @return the currentCapacity
     */
    public double getCurrentCapacity() {
        return currentCapacity;
    }

    /**
     * @param currentCapacity
     *            the currentCapacity to set
     */
    public void setCurrentCapacity(double currentCapacity) {
        if (currentCapacity > maxCapacity) {
            this.currentCapacity = this.maxCapacity;
        } else {
            this.currentCapacity = currentCapacity;
        }
    }

    /**
     * @return the maxCapacity
     */
    public double getMaxCapacity() {
        return maxCapacity;
    }
}
