/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Start Phase for the smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class StartPhase extends Flightphase {

    /**
     * the duration of the startphase (in seconds)
     */
    private static final double DURATION = 120;
    
    /**
     * the horizontal distance traveled in startphase (in meters)
     */
    private static final double HORIZONTAL_DISTANCE = 3704;
    
    /**
     * performance at the propeller (in kilowatt)
     */
    private static final double KW = 160;

    public StartPhase() {
        super(KW, FlightPhaseType.TKOF);
    }

    @Override
    public double getFlightTime() {
        return DURATION;
    }

    @Override
    public double getHorizontalDistance() {
        return HORIZONTAL_DISTANCE;
    }

    @Override
    public double getRateOfClimb() {
        return 0;
    }

    @Override
    public double getSpeed() {
        return HORIZONTAL_DISTANCE / DURATION;
    }
}
