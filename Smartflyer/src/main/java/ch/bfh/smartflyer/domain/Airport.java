/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * Airport class containing the airport specific attributes. Each airport is
 * unique based on the icao and iata code.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Airport {

    /**
     * the id of the aiport
     */
    private int id;

    /**
     * the name of the airport (often the city where the airport is located)
     */
    private String name;

    /**
     * the IATA code of the airport
     * 
     * IATA-Codes are given by the International Air Transport Association. The
     * code consists of three letters to identify each airport uniquely.
     */
    private String iata;

    /**
     * the ICAO code of the airport
     * 
     * IATA-Codes are given by the International Civil Aviation Organization.
     * The code consists of four letters to identify each airport uniquely.
     */
    private String icao;

    /**
     * latitude of the airport location
     */
    private double latitude;

    /**
     * longitude of the airport location
     */
    private double longitude;

    /**
     * Constructor for the airport class.
     * 
     * @param id
     *            the id of the airport
     * @param name
     *            the name of the airport (often the city)
     * @param iata
     *            the iata code of the airport
     * @param icao
     *            the icao code of the airport
     * @param latitude
     *            the longitude of the airport location
     * @param longitude
     *            the latitude of the airport location
     */
    public Airport(int id, String name, String iata, String icao,
            double latitude, double longitude) {
        super();
        this.id = id;
        this.name = name;
        this.iata = iata;
        this.icao = icao;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Getter and Setters

    public String getName() {
        return name;
    }

    public String getIata() {
        return iata;
    }

    public String getIcao() {
        return icao;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name + " " + iata + "/" + icao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((icao == null) ? 0 : icao.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Airport other = (Airport) obj;
        if (icao == null) {
            if (other.icao != null) {
                return false;
            }
        } else if (!icao.equals(other.icao)) {
            return false;
        }
        return true;
    }
}
