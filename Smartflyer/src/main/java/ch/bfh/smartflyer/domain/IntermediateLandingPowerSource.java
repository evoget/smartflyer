/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Intermediate Landing Phase with a Power Source
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class IntermediateLandingPowerSource extends Flightphase {

    // kW of power source (230V * 15A)
    /**
     * the power of the power source
     */
    private static final double POWER_SOURCE_POWER = 230.0 * 15.0 / 1000.0;

    /**
     * the length (duration) of the stay (in minutes)
     */
    private double lengthOfStay;

    /**
     * Creates a new Intermediate Landing Phase with a Power Source
     * 
     * @param lengthOfStay
     *            the duration of the stay
     */
    public IntermediateLandingPowerSource(double lengthOfStay) {
        super(-(POWER_SOURCE_POWER), FlightPhaseType.AIRPORT_STOP_POWERSOURCE);
        this.lengthOfStay = lengthOfStay;
    }

    @Override
    public double getFlightTime() {
        return lengthOfStay;
    }

    @Override
    public double getHorizontalDistance() {
        return 0;
    }

    @Override
    public double getRateOfClimb() {
        return 0;
    }

    @Override
    public double getSpeed() {
        return 0;
    }
}
