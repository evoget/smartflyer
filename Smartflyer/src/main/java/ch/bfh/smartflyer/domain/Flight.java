/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

import java.util.ArrayList;
import java.util.List;

import ch.bfh.smartflyer.support.AirportDistanceCalculator;

/**
 * A Flight for the smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Flight {
    
    /**
     * the airport where the flight starts
     */
    private Airport takeoff;
    
    /**
     * the airport where the flight ends
     */
    private Airport destination;

    /**
     * set of all via airports, can be none or several
     */
    private List<AirportVia> vias = new ArrayList<>();

    /**
     * the pilot, flying the plane
     */
    private Pilot pilot;

    /**
     * the list with all flight phases
     */
    private List<Flightphase> flightphases = new ArrayList<>();
    private double altitude;

    /**
     * Creates a flight for the smartflyer
     * 
     * @param takeoff
     *            the takeoff airport
     * @param destination
     *            the destination airport
     * @param vias
     *            the via-airports
     * @param pilot
     *            the pilot
     * @param altitude
     *            the altitude
     */
    public Flight(Airport takeoff, Airport destination, List<AirportVia> vias,
            Pilot pilot, double altitude) {
        super();
        this.takeoff = takeoff;
        this.destination = destination;
        this.vias = vias;
        this.pilot = pilot;
        this.altitude = altitude;

        /*
         * Creates the flightphases for this flight
         */

        // no via airports
        if (this.vias.isEmpty()) {

            Flightphase start = new StartPhase();
            Flightphase climb = new ClimbPhase(altitude);
            Flightphase des = new DecendPhase(this.altitude);

            this.flightphases.add(start);
            this.flightphases.add(climb);

            double distance = AirportDistanceCalculator.calculateDistance(
                    this.takeoff.getLatitude(), this.takeoff.getLongitude(),
                    this.destination.getLatitude(),
                    this.destination.getLongitude());

            this.flightphases.add(new CruisePhase(distance
                    - (start.getHorizontalDistance()
                            + climb.getHorizontalDistance() + des
                                .getHorizontalDistance()), this.altitude));
            this.flightphases.add(des);

            // via airports
        } else {

            List<AirportVia> viasWithDest = new ArrayList<>();
            for (AirportVia airportVia : vias)
                viasWithDest.add(airportVia);
            viasWithDest.add(new AirportVia(destination, 0, true));

            Airport lastAirport = this.takeoff;
            int count = 1;
            for (AirportVia airportVia : viasWithDest) {
                Flightphase startPhase = new StartPhase();
                Flightphase climbPhase = new ClimbPhase(altitude);
                Flightphase descendPhase = new DecendPhase(altitude);
                Flightphase intermediateLanding;
                if (airportVia.getPowerSource())
                    intermediateLanding = new IntermediateLandingPowerSource(
                            airportVia.getLengthOfStay());
                else
                    intermediateLanding = new IntermediateLanding(
                            airportVia.getLengthOfStay());
                this.flightphases.add(startPhase);
                this.flightphases.add(climbPhase);

                double distance = AirportDistanceCalculator.calculateDistance(
                        lastAirport.getLatitude(), lastAirport.getLongitude(),
                        airportVia.getAirport().getLatitude(), airportVia
                                .getAirport().getLongitude());
                distance -= (startPhase.getHorizontalDistance()
                        + climbPhase.getHorizontalDistance() + descendPhase
                        .getHorizontalDistance());
                this.flightphases.add(new CruisePhase(distance, this.altitude));
                this.flightphases.add(descendPhase);
                if (airportVia.getAirport() != this.destination
                        || count < viasWithDest.size())
                    this.flightphases.add(intermediateLanding);
                lastAirport = airportVia.getAirport();
                count++;
            }
        }
    }

    // Getters and Setters
    
    public Airport getTakeoff() {
        return takeoff;
    }

    public void setTakeoff(Airport takeoff) {
        this.takeoff = takeoff;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public void setVias(List<AirportVia> vias) {
        this.vias = vias;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public void setFlightphases(List<Flightphase> flightphases) {
        this.flightphases = flightphases;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public Airport getDestination() {
        return destination;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public List<Flightphase> getFlightphases() {
        return flightphases;
    }

    public double getAltitude() {
        return altitude;
    }

    public List<AirportVia> getVias() {
        return vias;
    }

}
