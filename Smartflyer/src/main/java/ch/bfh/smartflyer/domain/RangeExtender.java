/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

/**
 * A Range Extender for the smartflyer.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class RangeExtender {

    /**
     * power of the REX
     */
    private double power;

    /**
     * efficiency of the REX
     */
    private double efficiency;

    /**
     * if the REX is enabled or not
     */
    private boolean enabled = false;

    /**
     * Creates a new Range Extender for the smartflyer
     * 
     * @param power
     *            the power of the range extender
     * @param efficiency
     *            the efficiency of the range extender
     */
    public RangeExtender(double power, double efficiency) {
        super();
        this.power = power;
        if (efficiency > 0.01) {
            this.efficiency = efficiency;
        } else {
            this.efficiency = 1;
        }
    }

    /**
     * @return the power
     */
    public double getPower() {
        return power * efficiency;
    }

    /**
     * @return the petrol consumption
     */
    public double getPetrolConsumption() {
        return ((power / efficiency) / 3600) / 8.9;

    }

    public void enableRangeExtender() {
        this.enabled = true;
    }

    public void disableRangeExtender() {
        this.enabled = false;
    }

    public boolean getEnabled() {
        return enabled;
    }
}
