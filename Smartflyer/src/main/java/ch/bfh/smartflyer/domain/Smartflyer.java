/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

import java.util.List;

/**
 * An Interface for a smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public interface Smartflyer {
    
    public double getMaxElectromotorPower();

    public double getCurrentElectromotorPower();

    public double getRangeExtenderPower();

    public double getMaxBatteryCapacity();

    public double getCurrentBatteryCapacity();

    public double getMaxTankCapacity();

    public double getCurrentTankCapacity();

    public double getCurrentSpeed();

    public void startSimulation();

    public void stopSimulation();

    public boolean isStarted();

    public void setTimeAcceleration(int timeAcceleration);

    public double getAmbientTemperature();

    public boolean getRangeExtenderEnabled();

    public double getBatteryDistanceRemaining();

    public int getFlightDuration();

    public Flightphase getCurrentFlightphase();

    public double getFlightTimeRemaining();

    public double getCurrentAltitude();

    public double getTankTimeRemaining();

    public double getBatteryTimeRemaining();

    public void enableRangeExtender();

    public void disableRangeExtender();

    public void setAutomaticRangeExtender();

    public int getFlightphaseTimeRemaining();

    public boolean getAutomaticRangeExtender();

    public int flightTimeRemaining();

    public List<Double> getRealBatteryCapacity();

    public int getFlyingSeconds();
}
