/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.util.ArrayList;
import java.util.List;

import ch.bfh.smartflyer.business.FlightPlanner;
import ch.bfh.smartflyer.business.Simulator;
import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.AirportVia;
import ch.bfh.smartflyer.domain.Battery;
import ch.bfh.smartflyer.domain.Flight;
import ch.bfh.smartflyer.domain.Pilot;
import ch.bfh.smartflyer.domain.RangeExtender;
import ch.bfh.smartflyer.domain.Smartflyer;
import ch.bfh.smartflyer.domain.Tank;
import ch.bfh.smartflyer.support.BatteryCapacityUnterAuxiliaryException;
import ch.bfh.smartflyer.support.FlightTooShortException;
import ch.bfh.smartflyer.support.NotEnoughPetrolException;

/**
 * Controller for the Smartflyer
 * 
 * @author Thomas Vogel und Philip Vandermeulen
 *
 */
public class SmartflyerController {
    
    private Smartflyer smartflyer;
    private FlightPlanner flightplanner;
    private Flight flight;
    private RangeExtender rangeExtender;
    private Tank tank;
    private Battery battery;
    private List<AirportVia> viaList = new ArrayList<>();
    private Pilot pilot;
    private Double altitude = null;
    private boolean builded = false;
    private Airport takeoff;
    private Airport destination;

    public void setVias(List<AirportVia> viaList) {
        this.viaList = viaList;
    }

    public void setTakeoff(Airport takeoff) {
        this.takeoff = takeoff;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public void setRangeExtender(double power, double efficiency) {
        this.rangeExtender = new RangeExtender(power, efficiency);
    }

    public void setTank(double capacity) {
        this.tank = new Tank(capacity, capacity);

    }

    public void setBattery(double capacity) {
        this.battery = new Battery(capacity, capacity);
    }

    /**
     * Creates a flight
     * 
     * @return flight successfully created
     */
    public boolean createFlight() {
        boolean successfull = false;
        if (checkValues()) {
            this.flight = new Flight(takeoff, destination, viaList, pilot,
                    altitude);
            successfull = true;

        }
        return successfull;
    }

    /**
     * Check if the values to create a flight are not null
     * 
     * @return true if the values to create a flight aren't null
     */
    private boolean checkValues() {
        if (this.takeoff == null) {
            return false;
        } else if (this.destination == null) {
            return false;
        } else if (this.viaList == null) {
            return false;
        } else if (this.altitude == null) {
            return false;
        } else if (this.pilot == null) {
            return false;
        }
        return true;
    }

    /**
     * Creates the FlightPlanner
     * 
     * @return FlightPlanner successfully created
     * @throws FlightTooShortException
     * @throws NotEnoughPetrolException
     * @throws BatteryCapacityUnterAuxiliaryException
     */
    public boolean createFlightPlanner() throws FlightTooShortException,
            NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException {
        boolean successfull = false;
        if (this.flight != null && this.rangeExtender != null
                && this.battery != null && this.tank != null) {

            this.flightplanner = new FlightPlanner(flight, rangeExtender,
                    battery, tank);
            successfull = true;
            battery.setCurrentCapacity(battery.getMaxCapacity());
            tank.setCurrentCapacity(tank.getMaxCapacity());

        }
        return successfull;
    }

    /**
     * Creates the smartflyer
     * 
     * @return smartflyer successfully created
     */
    public boolean createSmartflyer() {
        boolean successfull = false;
        if (this.flight != null && this.rangeExtender != null
                && this.battery != null && this.tank != null) {
            this.smartflyer = new Simulator(flight, rangeExtender, battery,
                    tank);
            smartflyer.startSimulation();
            successfull = true;
        }
        builded = true;
        return successfull;

    }

    public Smartflyer getSmartflyer() {
        return smartflyer;
    }

    public boolean getBuilded() {
        return builded;
    }

    public FlightPlanner getFlightPlanner() {
        return flightplanner;
    }

    /**
     * Clears the controllers data
     */
    public void clearData() {
        this.viaList.clear();
    }

}
