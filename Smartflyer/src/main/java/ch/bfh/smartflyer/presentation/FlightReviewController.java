/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;

import org.apache.log4j.Logger;

/**
 * FXML Controller class for the Flight Review
 *
 * @author Thomas Vogel, Philip Vandermeulen
 */
public class FlightReviewController implements Initializable, ControlledScreen {

    private static final Logger LOGGER = Logger
            .getLogger(FlightReviewController.class);

    private boolean simulationRunning = true;
    private boolean present = false;
    private static final int DATAPOINTS_IN_GRAPH = 200;
    private SmartflyerController smartflyerController;
    private ScreensController myController;

    ObservableList<XYChart.Series<Double, Double>> lineChartData = FXCollections
            .observableArrayList();

    // ====================================================
    // JavaFx Components
    // ====================================================

    @FXML
    private LineChart<Double, Double> plannedFlightGraph;
    @FXML
    private Button goToSimulation;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private NumberAxis xAxis;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        plannedFlightGraph.setCreateSymbols(false);

        // New thread to wait for the planned line-chart-data
        new Thread() {
            @Override
            public void run() {
                while (simulationRunning) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            // Draw planned data if smartflyer is builded
                            if (smartflyerController.getBuilded()) {
                                simulationRunning = false;
                                drawPlanned();
                                return;
                            }
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LOGGER.error(
                                "Could not review flight, failed to draw chart.",
                                e);
                    }
                }
            }
        }.start();

        LineChart.Series<Double, Double> series1 = new LineChart.Series<Double, Double>();
        series1.setName("Real Flight");

        // New thread to draw the line-chart real line-chart-data
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (present
                            && smartflyerController.getBuilded()
                            && !smartflyerController.getSmartflyer()
                                    .isStarted()) {
                        // Draw the line-chart-data if the flight is finished

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                int count = 1;
                                int modulo = smartflyerController
                                        .getSmartflyer()
                                        .getRealBatteryCapacity().size() / 100;
                                for (Double value : smartflyerController
                                        .getSmartflyer()
                                        .getRealBatteryCapacity()) {
                                    if (count % modulo == 0) {
                                        series1.getData()
                                                .add(new XYChart.Data<Double, Double>(
                                                        (double) count, value));
                                    }
                                    count++;
                                }
                                lineChartData.add(series1);
                                plannedFlightGraph.setData(lineChartData);

                            }
                        });
                        return;

                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LOGGER.error(
                                "Failed to draw line chart in flight review.",
                                e);
                    }
                }
            }
        }.start();
    }

    /**
     * Draw the planned line-chart-data
     */
    private void drawPlanned() {

        LineChart.Series<Double, Double> series1 = new LineChart.Series<Double, Double>();
        series1.setName("Planned Flight");

        // New thread
        new Thread() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        int count = 1;
                        int modulo = smartflyerController.getFlightPlanner()
                                .getPlannedBatteryCapacity().size()
                                / DATAPOINTS_IN_GRAPH;
                        for (Double value : smartflyerController
                                .getFlightPlanner().getPlannedBatteryCapacity()) {
                            if (count % modulo == 0) {
                                series1.getData().add(
                                        new XYChart.Data<Double, Double>(
                                                (double) count, value));
                            }
                            count++;
                        }
                        lineChartData.add(series1);

                        plannedFlightGraph.setData(lineChartData);
                    }
                });
            }
        }.start();
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

    @Override
    public void setSmartFlyerController(
            SmartflyerController smartflyerController) {
        this.smartflyerController = smartflyerController;
        present = true;
    }

    @FXML
    void goToSimulation(ActionEvent event) {
        myController.setScreen(ScreensFramework.SCREEN2_ID);
    }
}