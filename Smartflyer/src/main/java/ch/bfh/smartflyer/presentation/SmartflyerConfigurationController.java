/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.net.URL;
import java.text.NumberFormat;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

import org.apache.log4j.Logger;

import ch.bfh.smartflyer.support.SmartflyerException;

/**
 * FXML Controller class
 *
 * @author Thomas Vogel und Philip Vandermeulen
 */
public class SmartflyerConfigurationController implements Initializable,
        ControlledScreen {

    private static final Logger LOGGER = Logger
            .getLogger(SmartflyerConfigurationController.class);

    // ====================================================
    // JavaFx Components
    // ====================================================

    @FXML
    private Label lblCurrentEfficiency;

    @FXML
    private Label lblError;

    @FXML
    private Slider sldREX;

    @FXML
    private Label lblCurrentBattery;

    @FXML
    private Slider sldREXEfficiency;

    @FXML
    private Slider sldBattery;

    @FXML
    private Slider sldPetrol;

    @FXML
    private Label lblCurrentPetrol;

    @FXML
    private GridPane gridPanePassengers;

    @FXML
    private TextField txtBaggage;

    @FXML
    private Label lblCurrentREX;

    SmartflyerController smartflyerController;
    ScreensController myController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtBaggage.addEventFilter(KeyEvent.KEY_TYPED,
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent inputevent) {
                        if (!inputevent.getCharacter().matches("\\d")) {
                            inputevent.consume();
                        }
                    }
                });

        sldBattery.valueProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            NumberFormat nf = NumberFormat.getInstance();
                            nf.setMaximumFractionDigits(1);
                            lblCurrentBattery.setText(nf.format(newValue
                                    .doubleValue()));
                        });
        sldPetrol.valueProperty().addListener(
                (observable, oldValue, newValue) -> lblCurrentPetrol
                        .setText(newValue.intValue() + ""));
        sldREX.valueProperty().addListener(
                (observable, oldValue, newValue) -> lblCurrentREX
                        .setText(newValue.intValue() + ""));
        sldREXEfficiency.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    NumberFormat nf = NumberFormat.getInstance();
                    nf.setMaximumFractionDigits(2);
                    lblCurrentEfficiency.setText(nf.format(newValue
                            .doubleValue()));
                });
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

    /**
     * Go to flight configuration
     * 
     * @param event
     */
    @FXML
    void configureFlight(ActionEvent event) {
        this.smartflyerController.clearData();
        lblError.setText("");
        myController.setScreen(ScreensFramework.SCREEN1_ID);
    }

    /**
     * Starts the flight simulation
     * 
     * @param event
     */
    @FXML
    void startFlight(ActionEvent event) {

        this.smartflyerController.setBattery(sldBattery.getValue());
        this.smartflyerController.setRangeExtender((int) sldREX.getValue(),
                sldREXEfficiency.getValue());
        this.smartflyerController.setTank(sldPetrol.getValue());

        try {
            this.smartflyerController.createFlightPlanner();
        } catch (SmartflyerException e) {
            LOGGER.info("Could not start Flight", e);
            lblError.setText(e.printFailure());
            return;
        }

        this.smartflyerController.createSmartflyer();

        myController.setScreen(ScreensFramework.SCREEN2_ID);
    }

    @Override
    public void setSmartFlyerController(
            SmartflyerController smartflyerController) {
        this.smartflyerController = smartflyerController;

    }
}
