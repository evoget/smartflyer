/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * JavaFX Multiscreen Framework based on JFX-MultiScreen from Angela Caicedo
 * {@link https://github.com/acaicedo/JFX-MultiScreen}
 * 
 * @author Thomas Vogel und Philip Vandermeulen
 */
public class ScreensFramework extends Application {

    public static final String SCREEN1_ID = "FlightPlanner";
    public static final String SCREEN1_FILE = "FlightPlanner.fxml";
    public static final String SCREEN2_ID = "FlightSimulation";
    public static final String SCREEN2_FILE = "FlightSimulation.fxml";
    public static final String SCREEN3_ID = "FlightReview";
    public static final String SCREEN3_FILE = "FlightReview.fxml";
    public static final String SCREEN4_ID = "SmartflyerConfiguration";
    public static final String SCREEN4_FILE = "SmartflyerConfiguation.fxml";
    public static final String SCREEN5_ID = "NoDatabaseServer";
    public static final String SCREEN5_FILE = "NoDatabaseServer.fxml";

    @Override
    public void start(Stage primaryStage) {

        ScreensController mainContainer = new ScreensController();

        SmartflyerController smartflyerController = new SmartflyerController();

        mainContainer.loadScreen(ScreensFramework.SCREEN5_ID,
                ScreensFramework.SCREEN5_FILE, smartflyerController);
        mainContainer.loadScreen(ScreensFramework.SCREEN1_ID,
                ScreensFramework.SCREEN1_FILE, smartflyerController);
        mainContainer.loadScreen(ScreensFramework.SCREEN2_ID,
                ScreensFramework.SCREEN2_FILE, smartflyerController);
        mainContainer.loadScreen(ScreensFramework.SCREEN3_ID,
                ScreensFramework.SCREEN3_FILE, smartflyerController);
        mainContainer.loadScreen(ScreensFramework.SCREEN4_ID,
                ScreensFramework.SCREEN4_FILE, smartflyerController);

        if (mainContainer.dbConnectionOk())
            mainContainer.setScreen(ScreensFramework.SCREEN1_ID);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
        primaryStage.setScene(scene);

        primaryStage.show();
        primaryStage.setWidth(922);
        primaryStage.setHeight(620);
        primaryStage.setResizable(false);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
