/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller Class
 * 
 * @author Thomas
 *
 */
public class NoDatabaseServerController implements Initializable,
        ControlledScreen {

    // ====================================================
    // JavaFx Components
    // ====================================================

    @FXML
    private Button btnExit;
    private ScreensController myController;
    private SmartflyerController smartflyerController;

    @FXML
    void closeApplication(ActionEvent event) {
        System.exit(0);
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        this.myController = screenParent;
    }

    @Override
    public void setSmartFlyerController(
            SmartflyerController smartflyerController) {
        this.smartflyerController = smartflyerController;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // not used
    }
}
