/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

/**
 * Interface for a controlled JavaFX screen based on JFX-MultiScreen from Angela Caicedo
 * {@link https://github.com/acaicedo/JFX-MultiScreen}
 * @author Thomas Vogel und Philip Vandermeulen
 */
public interface ControlledScreen {
    //This method will allow the injection of the Parent ScreenPane
    public void setScreenParent(ScreensController screenPage);
    public void setSmartFlyerController(SmartflyerController smartflyerController);
}
