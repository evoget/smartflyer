/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

import org.apache.log4j.Logger;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.AirportVia;
import ch.bfh.smartflyer.domain.Pilot;
import ch.bfh.smartflyer.persistence.DBQuery;
import ch.bfh.smartflyer.persistence.DBQueryImpl;
import ch.bfh.smartflyer.persistence.JDBCConnector;
import ch.bfh.smartflyer.support.FxUtil;

/**
 * FXML Controller class for the Flight Planner
 *
 * @author Thomas Vogel, Philip Vandermeulen
 */
public class FlightPlannerController implements Initializable, ControlledScreen {

    private static final Logger LOGGER = Logger
            .getLogger(FlightPlannerController.class);

    // ====================================================
    // JavaFx Components
    // ====================================================

    @FXML
    private ComboBox<Pilot> comboPilot;

    @FXML
    private TextField txtLengthOfStay;

    @FXML
    private CheckBox checkPowerSource;

    @FXML
    private ComboBox<Integer> comboAltitude;

    @FXML
    private ComboBox<Airport> comboDestination;

    @FXML
    private ComboBox<Airport> comboDeparture;

    @FXML
    private Button btnClear;

    @FXML
    private GridPane gridLayout1;

    @FXML
    private Button btnVia = new Button();

    @FXML
    private Label lblError;

    @FXML
    private Label lblStay;

    private List<AirportVia> viaList = new ArrayList<>();
    private List<ComboBox<Airport>> viaBoxes = new ArrayList<>();
    private List<TextField> viaTxtsHours = new ArrayList<>();
    private List<CheckBox> viaChecks = new ArrayList<>();
    private List<TextField> viaTxtsMinutes = new ArrayList<>();

    private ObservableList<Airport> airports = FXCollections
            .observableArrayList();

    private SmartflyerController smartflyerController;

    private ScreensController myController;

    private DBQuery db;

    private boolean databaseError = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnClear.setVisible(false);
        lblStay.setVisible(false);

        // initialize altitude dropdown with values
        ObservableList<Integer> altitudes = FXCollections.observableArrayList(
                3000, 6000, 9000);

        ObservableList<Pilot> pilots = FXCollections.observableArrayList();

        try {
            db = new DBQueryImpl(new JDBCConnector().getConnection());
        } catch (Exception e) {
            LOGGER.error("Could not connect to Database", e);
            databaseError = true;
        }

        if (!databaseError) {
            // get all airports and pilots from database
            List<Airport> dbAirports = db.getAllAirports();
            List<Pilot> dbPilots = db.getAllPilots();

            airports.addAll(dbAirports);
            pilots.addAll(dbPilots);

            comboDeparture.setItems(airports);
            comboDestination.setItems(airports);

            FxUtil.autoCompleteComboBox(comboDeparture,
                    FxUtil.AutoCompleteMode.CONTAINING);
            FxUtil.autoCompleteComboBox(comboDestination,
                    FxUtil.AutoCompleteMode.CONTAINING);

            comboAltitude.setItems(altitudes);
            comboAltitude.setValue(altitudes.get(0));

            comboPilot.setItems(pilots);
            comboPilot.setValue(pilots.get(0));
        }
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;

        // Check for DB-Errors
        if (databaseError)
            myController.setScreen(ScreensFramework.SCREEN5_ID);
        else
            myController.setDbConnectionOk();

    }

    @FXML
    void startConfiguration(ActionEvent event) {

        int count = 0;
        for (TextField field : viaTxtsHours) {

            if (("").equals(viaTxtsMinutes.get(count).getText())) {
                viaTxtsMinutes.get(count).setText("0");
            }
            if (("").equals(field.getText())) {
                field.setText("0");
            }
            count++;
        }

        count = 0;
        for (ComboBox<Airport> viaBox : viaBoxes) {
            viaList.add(new AirportVia(
                    FxUtil.getComboBoxValue(viaBox),
                    (new Integer(viaTxtsHours.get(count).getText()) * 3600)
                            + (new Integer(viaTxtsMinutes.get(count).getText()) * 60),
                    viaChecks.get(count).isSelected()));
            count++;
        }

        if (FxUtil.getComboBoxValue(comboDeparture) != FxUtil
                .getComboBoxValue(comboDestination) || !viaList.isEmpty()) {

            this.smartflyerController
                    .setAltitude(comboAltitude.getValue() / 3.2808);
            this.smartflyerController.setPilot(comboPilot.getValue());
            this.smartflyerController.setTakeoff(FxUtil
                    .getComboBoxValue(comboDeparture));
            this.smartflyerController.setDestination(FxUtil
                    .getComboBoxValue(comboDestination));
            this.smartflyerController.setVias(viaList);
            if (!this.smartflyerController.createFlight()) {
                lblError.setText("Please select valid airports!");
            } else {

                // insert flight into database
                db.insertFlight(comboPilot.getValue().getId(), FxUtil
                        .getComboBoxValue(comboDeparture).getId(), FxUtil
                        .getComboBoxValue(comboDestination).getId(), new Date());

                myController.setScreen(ScreensFramework.SCREEN4_ID);
            }
        } else {
            lblError.setText("Please verify your flight!");
        }

    }

    @FXML
    private void addVia(ActionEvent event) {

        lblStay.setVisible(true);
        btnClear.setVisible(true);
        final Label label = new Label(viaBoxes.size() + 1 + ". Via ");
        label.setMinWidth(222);
        label.setPadding(new Insets(0, 190, 0, 0));
        final ComboBox<Airport> newCombo = new ComboBox<Airport>(airports);
        FxUtil.autoCompleteComboBox(newCombo,
                FxUtil.AutoCompleteMode.CONTAINING);
        viaBoxes.add(newCombo);
        newCombo.setValue(airports.get(viaBoxes.size()));
        newCombo.setPrefWidth(150);
        final TextField txtField = new TextField();
        txtField.addEventFilter(KeyEvent.KEY_TYPED,
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent inputevent) {
                        if (!inputevent.getCharacter().matches("\\d")) {
                            inputevent.consume();
                        }
                    }
                });
        txtField.setMaxWidth(100);
        txtField.setPromptText("hours");
        viaTxtsHours.add(txtField);
        final TextField txtFieldMinutes = new TextField();
        txtFieldMinutes.addEventFilter(KeyEvent.KEY_TYPED,
                new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent inputevent) {
                        if (!inputevent.getCharacter().matches("\\d")) {
                            inputevent.consume();
                        }
                    }
                });
        txtFieldMinutes.setMaxWidth(100);
        txtFieldMinutes.setPromptText("minutes");
        viaTxtsMinutes.add(txtFieldMinutes);

        final CheckBox checkBox = new CheckBox("Power Source");
        viaChecks.add(checkBox);

        GridPane grid = new GridPane();
        grid.setHgap(20);
        grid.setMinWidth(860);
        grid.add(label, 0, 0);
        grid.add(newCombo, 1, 0);
        grid.add(txtField, 2, 0);
        grid.add(txtFieldMinutes, 3, 0);
        grid.add(checkBox, 4, 0);

        gridLayout1.addRow(viaBoxes.size() - 1, grid);

        if (viaBoxes.size() > 4) {
            btnVia.setVisible(false);
            btnClear.setVisible(true);
        }

    }

    @FXML
    private void clearVias(ActionEvent event) {
        lblStay.setVisible(false);
        viaChecks.clear();
        viaTxtsHours.clear();
        viaTxtsMinutes.clear();
        viaBoxes.clear();
        viaList.clear();
        gridLayout1.getChildren().removeAll(gridLayout1.getChildren());
        btnClear.setVisible(false);
        btnVia.setVisible(true);
    }

    @FXML
    private void goToScreen2(ActionEvent event) {
        myController.setScreen(ScreensFramework.SCREEN2_ID);
    }

    @FXML
    private void goToScreen3(ActionEvent event) {
        myController.setScreen(ScreensFramework.SCREEN3_ID);
    }

    @Override
    public void setSmartFlyerController(
            SmartflyerController smartflyerController) {
        this.smartflyerController = smartflyerController;

    }
}
