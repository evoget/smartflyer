/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.presentation;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import ch.bfh.smartflyer.domain.FlightPhaseType;
import ch.bfh.smartflyer.domain.Smartflyer;

/**
 * FXML Controller for the Flight Simulation
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class FlightSimulationController implements Initializable,
        ControlledScreen {

    private static final Logger LOGGER = Logger
            .getLogger(FlightSimulationController.class);

    private Image imgBattery100;
    private Image imgBattery80;
    private Image imgBattery60;
    private Image imgBattery30;
    private Image imgBattery0;
    private Image imgPetrol;
    private Image imgPowerBattery;
    private Image imgPowerLandedRex;
    private Image imgPowerLandedRexPowerSource;
    private Image imgPowerLandedPowerSource;
    private Image imgPowerRex;
    private Image imgPowerNone;
    private Image imgFlightphaseStart;
    private Image imgFlightphaseClimb;
    private Image imgFlightphaseCruise;
    private Image imgFlightphaseDescend;
    private Image imgFlightphaseLanded;

    // ====================================================
    // JavaFx Components
    // ====================================================
    @FXML
    private Label lblSpeed;
    @FXML
    private Label lblAltitude;
    @FXML
    private Label lblRangeExtenderPower;
    @FXML
    private Button btnReview;
    @FXML
    private Label lblCurrentPetrol;
    @FXML
    private Slider sldTimeAcceleration;
    @FXML
    private Label lblPowerConsumption;
    @FXML
    private Label lblDistanceRemaining;
    @FXML
    private Label lblRexMode;
    @FXML
    private Label lblTimeRemaingFlightphase;
    @FXML
    private Label lblDistanceRemainingBattery;
    @FXML
    private Label lblTimeAcceleration;
    @FXML
    private Label lblBatteryTimeRemaining;
    @FXML
    private Label lblPetrolTimeRemaining;
    @FXML
    private ImageView flightphaseImage;
    @FXML
    private ImageView powerImage;
    @FXML
    private Label labelFlightphase;
    @FXML
    private Label lblTemperature;
    @FXML
    private Label labelCurrentPetrol;
    @FXML
    private Label lblCurrentBattery;
    @FXML
    private Label lblTimeRemaining;
    @FXML
    private Label lblDifferenceBattery;
    @FXML
    private ImageView petrolImage;
    @FXML
    private ImageView batteryImage;
    @FXML
    private Label lblSpeedUnit;

    private SmartflyerController smartflyerController;
    private ScreensController myController;
    private Smartflyer smartflyer;
    private int timeAcceleration = 1;
    private int flyingSeconds = 0;
    private boolean simulationRunning = true;

    /**
     * string for empty time
     */
    private static final String EMPTY_TIME = "-- : -- : --";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sldTimeAcceleration.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    timeAcceleration = newValue.intValue();
                    lblTimeAcceleration.setText(timeAcceleration + " x");
                });

        // load the images
        loadImages();

        this.btnReview.setVisible(false);
        this.batteryImage.setImage(imgBattery100);
        this.petrolImage.setImage(imgPetrol);
        this.powerImage.setImage(imgPowerBattery);
        this.flightphaseImage.setImage(imgFlightphaseStart);

        // New thread for the flight simulation
        new Thread() {
            @Override
            public void run() {
                while (simulationRunning) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            // Start simulation only if smartflyer is builded
                            if (smartflyerController.getBuilded()) {
                                smartflyer = smartflyerController
                                        .getSmartflyer();

                                // Start simulation only if smartflyer is
                                // started
                                if (!smartflyer.isStarted()) {
                                    smartflyer.startSimulation();
                                }

                                simulationRunning = smartflyer.isStarted();

                                /*
                                 * Set labels and images for the appropriate
                                 * state of the simulation
                                 */
                                double currentBatteryPercent = 100.0
                                        / smartflyer.getMaxBatteryCapacity()
                                        * smartflyer
                                                .getCurrentBatteryCapacity();

                                lblDifferenceBattery.setText(roundDouble(currentBatteryPercent
                                        - (100.0 / smartflyer
                                                .getMaxBatteryCapacity() * smartflyerController
                                                .getFlightPlanner()
                                                .getPlannedBatteryCapacity()
                                                .get(smartflyer
                                                        .getFlyingSeconds() - 1))));
                                if (smartflyerController.getFlightPlanner()
                                        .getPlannedBatteryCapacity().size() > flyingSeconds)
                                    flyingSeconds++;

                                if (currentBatteryPercent > 80) {
                                    batteryImage.setImage(imgBattery100);
                                } else if (currentBatteryPercent > 60
                                        && currentBatteryPercent < 80) {
                                    batteryImage.setImage(imgBattery80);
                                } else if (currentBatteryPercent > 30
                                        && currentBatteryPercent < 60) {
                                    batteryImage.setImage(imgBattery60);
                                } else if (currentBatteryPercent < 30
                                        && currentBatteryPercent > 10) {
                                    batteryImage.setImage(imgBattery30);
                                } else {
                                    batteryImage.setImage(imgBattery0);
                                }

                                lblTimeRemaining.setText(""
                                        + getDurationString((int) smartflyer
                                                .getFlightTimeRemaining()));
                                double batteryRemaining = smartflyer
                                        .getBatteryTimeRemaining();

                                if (Double.compare(batteryRemaining,
                                        Double.POSITIVE_INFINITY) == 0
                                        || Double
                                                .compare(batteryRemaining, 0.0) < 0) {
                                    lblBatteryTimeRemaining.setText(EMPTY_TIME);
                                } else {
                                    lblBatteryTimeRemaining
                                            .setText(""
                                                    + getDurationString((int) batteryRemaining));
                                }

                                lblPetrolTimeRemaining.setText(""
                                        + getDurationString((int) smartflyer
                                                .getTankTimeRemaining()));
                                lblTemperature.setText(""
                                        + roundDouble(smartflyer
                                                .getAmbientTemperature()));
                                lblCurrentPetrol.setText(roundDoublePercent(100.0
                                        / smartflyer.getMaxTankCapacity()
                                        * smartflyer.getCurrentTankCapacity()));
                                lblCurrentBattery
                                        .setText(roundDoublePercent(currentBatteryPercent));
                                lblAltitude.setText(""
                                        + (int) smartflyer.getCurrentAltitude());
                                lblTimeRemaingFlightphase.setText(""
                                        + getDurationString(smartflyer
                                                .getFlightphaseTimeRemaining()));

                                if (smartflyer.getRangeExtenderEnabled()) {
                                    lblRangeExtenderPower.setText(""
                                            + roundDouble(smartflyer
                                                    .getRangeExtenderPower()));
                                } else {
                                    lblRangeExtenderPower.setText("0");
                                }

                                double batteryDistanceRemaining = smartflyer
                                        .getBatteryDistanceRemaining() / 1000;

                                if (batteryDistanceRemaining < 0) {
                                    batteryDistanceRemaining = 0;
                                }

                                lblDistanceRemainingBattery
                                        .setText(""
                                                + roundDouble(batteryDistanceRemaining));

                                if (smartflyer.getCurrentFlightphase()
                                        .getCurrentPowerConsumption() < 0) {
                                    lblPowerConsumption.setText("0");
                                } else {
                                    lblPowerConsumption
                                            .setText(""
                                                    + roundDouble(smartflyer
                                                            .getCurrentFlightphase()
                                                            .getCurrentPowerConsumption()));
                                }

                                lblSpeed.setText(""
                                        + roundDouble(smartflyer
                                                .getCurrentFlightphase()
                                                .getSpeed() * 3.6));

                                if (smartflyer.getCurrentFlightphase()
                                        .getFlightPhaseType() == FlightPhaseType.TKOF) {
                                    lblSpeed.setText("Starting ...");
                                } else {
                                    lblSpeedUnit.setVisible(true);
                                }

                                lblTimeAcceleration.setText(""
                                        + timeAcceleration + " x");
                                smartflyerController.getSmartflyer()
                                        .setTimeAcceleration(timeAcceleration);

                                if (smartflyer.getAutomaticRangeExtender()) {
                                    lblRexMode.setText("Auto");
                                } else {
                                    lblRexMode.setText("Manual");
                                }

                                Image currentPowerImage;
                                FlightPhaseType currentPhaseType = smartflyer
                                        .getCurrentFlightphase()
                                        .getFlightPhaseType();

                                if (smartflyer.getRangeExtenderEnabled()) {
                                    if (currentPhaseType == FlightPhaseType.AIRPORT_STOP_POWERSOURCE) {
                                        currentPowerImage = imgPowerLandedRexPowerSource;
                                    } else if (currentPhaseType == FlightPhaseType.AIRPORT_STOP) {
                                        currentPowerImage = imgPowerLandedRex;
                                    } else {
                                        currentPowerImage = imgPowerRex;
                                    }
                                } else {
                                    if (currentPhaseType == FlightPhaseType.AIRPORT_STOP) {
                                        currentPowerImage = imgPowerNone;
                                    } else if (currentPhaseType == FlightPhaseType.AIRPORT_STOP_POWERSOURCE) {
                                        currentPowerImage = imgPowerLandedPowerSource;
                                    } else {
                                        currentPowerImage = imgPowerBattery;
                                    }
                                }

                                Image currentFlightphaseImage;
                                switch (currentPhaseType) {
                                case CLB:
                                    labelFlightphase.setText("Climb");
                                    currentFlightphaseImage = imgFlightphaseClimb;
                                    break;
                                case CRS:
                                    labelFlightphase.setText("Cruise");
                                    currentFlightphaseImage = imgFlightphaseCruise;
                                    break;
                                case DES:
                                    labelFlightphase.setText("Descent");
                                    currentFlightphaseImage = imgFlightphaseDescend;
                                    break;
                                case AIRPORT_STOP:
                                    labelFlightphase.setText("Landed");
                                    currentFlightphaseImage = imgFlightphaseLanded;
                                    lblAltitude.setText("0");
                                    break;
                                case AIRPORT_STOP_POWERSOURCE:
                                    labelFlightphase.setText("Landed");
                                    currentFlightphaseImage = imgFlightphaseLanded;
                                    lblAltitude.setText("0");
                                    break;
                                default:
                                    labelFlightphase.setText("Start");
                                    currentFlightphaseImage = imgFlightphaseStart;
                                    break;
                                }

                                flightphaseImage
                                        .setImage(currentFlightphaseImage);
                                powerImage.setImage(currentPowerImage);
                            }
                        }
                    });
                    try {
                        // Simulates a flying-second
                        Thread.sleep(1000L / timeAcceleration);
                    } catch (InterruptedException e) {
                        LOGGER.error("Error in flight simulation", e);
                    }
                }
                // Flight finished
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        labelFlightphase.setText("Finished");
                        flightphaseImage.setImage(imgFlightphaseLanded);
                        lblAltitude.setText("0");
                        lblSpeed.setText("0");
                        lblRangeExtenderPower.setText("0");
                        powerImage.setImage(imgPowerNone);
                        lblPowerConsumption.setText("0");
                        lblBatteryTimeRemaining.setText(EMPTY_TIME);
                        lblPetrolTimeRemaining.setText(EMPTY_TIME);
                        lblDistanceRemainingBattery.setText("0");
                        btnReview.setVisible(true);
                    }
                });
            }
        }.start();
    }

    /**
     * Load the images
     */
    private void loadImages() {
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/akku_100.png"));
            imgBattery100 = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/akku_80.png"));
            imgBattery80 = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/akku_60.png"));
            imgBattery60 = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/akku_30.png"));
            imgBattery30 = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/akku_0.png"));
            imgBattery0 = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/startPhase.png"));
            imgFlightphaseStart = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/climbPhase.png"));
            imgFlightphaseClimb = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/cruisePhase.png"));
            imgFlightphaseCruise = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/descendPhase.png"));
            imgFlightphaseDescend = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/landedPhase.png"));
            imgFlightphaseLanded = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/petrol.png"));
            imgPetrol = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/powerFlow_battery.png"));
            imgPowerBattery = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/powerFlow_Landed_Rex.png"));
            imgPowerLandedRex = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO
                    .read(FlightSimulationController.class
                            .getResourceAsStream("/images/powerFlow_Landed_Rex_Power.png"));
            imgPowerLandedRexPowerSource = SwingFXUtils.toFXImage(
                    bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/powerFlow_rex.png"));
            imgPowerRex = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/powerFlow_none.png"));
            imgPowerNone = SwingFXUtils.toFXImage(bufferedImage, null);
            bufferedImage = ImageIO.read(FlightSimulationController.class
                    .getResourceAsStream("/images/powerFlow__PowerSource.png"));
            imgPowerLandedPowerSource = SwingFXUtils.toFXImage(bufferedImage,
                    null);

        } catch (IOException e) {
            LOGGER.error("Could not load images in FlightSimulationController",
                    e);
        }

    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        this.myController = screenParent;
    }

    @FXML
    private void goToScreen1(ActionEvent event) {
        this.myController.setScreen(ScreensFramework.SCREEN1_ID);
    }

    @FXML
    private void goToScreen3(ActionEvent event) {
        this.myController.setScreen(ScreensFramework.SCREEN3_ID);
    }

    @Override
    public void setSmartFlyerController(
            SmartflyerController smartflyerController) {
        this.smartflyerController = smartflyerController;
        this.smartflyer = this.smartflyerController.getSmartflyer();

    }

    private String roundDouble(double value) {
        DecimalFormat df = new DecimalFormat("0.#");
        return df.format(value);
    }

    private String roundDoublePercent(double value) {
        DecimalFormat df = new DecimalFormat("##.#");
        return df.format(value) + "%";
    }

    @FXML
    void disableRangeExtender(ActionEvent event) {
        smartflyer.disableRangeExtender();
    }

    @FXML
    void enableRangeExtender(ActionEvent event) {
        smartflyer.enableRangeExtender();
    }

    @FXML
    void autoRangeExtender(ActionEvent event) {
        smartflyer.setAutomaticRangeExtender();
    }

    @FXML
    void slower(ActionEvent event) {
        if (timeAcceleration > 1) {
            timeAcceleration--;
        }
        sldTimeAcceleration.setValue(timeAcceleration);

    }

    @FXML
    void faster(ActionEvent event) {
        if (timeAcceleration < 100) {
            timeAcceleration++;
        }
        sldTimeAcceleration.setValue(timeAcceleration);
    }

    @FXML
    void reviewFlight(ActionEvent event) {
        myController.setScreen(ScreensFramework.SCREEN3_ID);
    }

    /**
     * Returns a pretty time format
     * 
     * @param seconds
     *            Seconds to format
     * @return A pretty time string
     */
    private String getDurationString(int seconds) {
        if (seconds < 0) {
            return "00 : 00 : 00";
        }
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        int sec = seconds % 60;

        return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : "
                + twoDigitString(sec);
    }

    /**
     * Returns a two-digit string
     * 
     * @param number
     *            The number to format
     * @return A tow-digit string
     */
    private String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}
