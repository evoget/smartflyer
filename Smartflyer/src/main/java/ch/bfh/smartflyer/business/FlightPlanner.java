/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.business;

import java.util.ArrayList;
import java.util.List;

import ch.bfh.smartflyer.domain.AirportVia;
import ch.bfh.smartflyer.domain.Battery;
import ch.bfh.smartflyer.domain.ClimbPhase;
import ch.bfh.smartflyer.domain.DecendPhase;
import ch.bfh.smartflyer.domain.Electromotor;
import ch.bfh.smartflyer.domain.Flight;
import ch.bfh.smartflyer.domain.FlightPhaseType;
import ch.bfh.smartflyer.domain.Flightphase;
import ch.bfh.smartflyer.domain.RangeExtender;
import ch.bfh.smartflyer.domain.StartPhase;
import ch.bfh.smartflyer.domain.Tank;
import ch.bfh.smartflyer.support.AirportDistanceCalculator;
import ch.bfh.smartflyer.support.BatteryCapacityUnterAuxiliaryException;
import ch.bfh.smartflyer.support.FlightTooShortException;
import ch.bfh.smartflyer.support.NotEnoughBatteryException;
import ch.bfh.smartflyer.support.NotEnoughPetrolException;

/**
 * Flight Planner; plans a flight with a given range extender, battery and tank
 * well as a pilot.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class FlightPlanner {

    private static final int MIN_RANGE_EXTENDER_ALTITUDE = 600;
    private static final double MIN_AUXILIARY_BATTERY_TIME = 1.0 * 60.0; // 10
                                                                         // minutes
                                                                         // * 60
                                                                         // seconds
    private static final double BATTERY_LOAD_CAPACITY = 97;
    private List<Double> plannedBatteryCapacity = new ArrayList<>();
    private Flight flight;
    private int second;
    private double currentAltitude = 0;
    private Electromotor electromotor = new Electromotor(160, 0);
    private Battery battery;
    private RangeExtender rangeExtender;
    private Tank tank;

    /**
     * A Fligt Planner for a flight
     * 
     * @param flight
     *            The flight
     * @param rangeExtender
     *            The range extender
     * @param battery
     *            The battery
     * @param tank
     *            The tank
     * @throws FlightTooShortException
     * @throws NotEnoughBatteryException
     * @throws BatteryCapacityUnterAuxiliaryException
     * @throws NotEnoughPetrolException
     */
    public FlightPlanner(Flight flight, RangeExtender rangeExtender,
            Battery battery, Tank tank) throws FlightTooShortException,
            NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException {
        super();
        this.battery = battery;
        this.tank = tank;
        this.rangeExtender = rangeExtender;
        this.flight = flight;

        checkMinDistance();

        for (this.second = 1; this.second < getFlightTime(); this.second++) {

            Flightphase current = getCurrentFlightphase();

            currentAltitude += current.getRateOfClimb();
            if (currentAltitude < 0){
                currentAltitude = 0;
            }
            checkRangeExtender();
            this.electromotor.setCurrentPower(current
                    .getCurrentPowerConsumption());
            this.battery
                    .setCurrentCapacity((this.battery.getCurrentCapacity() * 1000 * 3600 - this.electromotor
                            .getCurrentPower() * 1000) / (1000 * 3600));
            if (this.rangeExtender.getEnabled()) {
                this.tank.setCurrentCapacity(this.tank.getCurrentCapacity()
                        - this.rangeExtender.getPetrolConsumption());

                if (this.tank.getCurrentCapacity() < 0) {
                    throw new NotEnoughPetrolException();
                }
                this.battery.setCurrentCapacity(this.battery
                        .getCurrentCapacity()
                        + (this.rangeExtender.getPower() / 3600));
            }
            this.plannedBatteryCapacity.add(battery.getCurrentCapacity());
            checkBatteryTimeRemaining();
        }
    }

    /**
     * Check the minimum distance for the flight to reach the planned altitude
     * 
     * @throws FlightTooShortException
     */
    private void checkMinDistance() throws FlightTooShortException {
        List<AirportVia> vias = new ArrayList<>();
        double smallestDistance = Double.MAX_VALUE;
        for (AirportVia via : flight.getVias()) {
            vias.add(via);
        }
        vias.add(new AirportVia(flight.getDestination(), 0, false));

        AirportVia latestVia = new AirportVia(flight.getTakeoff(), 0, false);
        for (AirportVia via : vias) {
            smallestDistance = Math.min(smallestDistance,
                    AirportDistanceCalculator.calculateDistance(latestVia
                            .getAirport().getLatitude(), latestVia.getAirport()
                            .getLongitude(), via.getAirport().getLatitude(),
                            via.getAirport().getLongitude()));
            latestVia = via;
        }

        Flightphase start = new StartPhase();
        Flightphase climb = new ClimbPhase(this.flight.getAltitude());
        Flightphase descend = new DecendPhase(this.flight.getAltitude());
        double minDistance = start.getHorizontalDistance()
                + climb.getHorizontalDistance()
                + descend.getHorizontalDistance();

        if (minDistance > smallestDistance){
            throw new FlightTooShortException();
        }
    }

    /**
     * Returns the current flightphase
     * 
     * @return The current flightphase
     */
    private Flightphase getCurrentFlightphase() {
        List<Flightphase> phases = flight.getFlightphases();
        Flightphase currentPhase = phases.get(0);
        int secondsHelper = 0;

        for (Flightphase flightphase : phases) {
            secondsHelper += flightphase.getFlightTime();
            if (this.second < secondsHelper) {
                currentPhase = flightphase;
                break;
            }
        }
        return currentPhase;
    }

    /**
     * Checks if the RangeExtender should be started
     * 
     * @throws BatteryCapacityUnterAuxiliaryException
     */
    private void checkRangeExtender() {

        if (currentAltitude >= MIN_RANGE_EXTENDER_ALTITUDE
                || getCurrentFlightphase().getFlightPhaseType() == FlightPhaseType.AIRPORT_STOP
                || getCurrentFlightphase().getFlightPhaseType() == FlightPhaseType.AIRPORT_STOP_POWERSOURCE) {
            if (Double.compare(battery.getCurrentCapacity(),
                    battery.getMaxCapacity()) == 0) {
                rangeExtender.disableRangeExtender();
            }
            if ((100 / battery.getMaxCapacity() * battery.getCurrentCapacity()) < BATTERY_LOAD_CAPACITY) {
                rangeExtender.enableRangeExtender();
            }
        } else {
            if (getBatteryTimeRemaining() < MIN_AUXILIARY_BATTERY_TIME
                    && Double.compare(battery.getCurrentCapacity(),
                            battery.getMaxCapacity()) != 0) {
                rangeExtender.enableRangeExtender();
            } else
                rangeExtender.disableRangeExtender();
        }

    }

    /**
     * Returns the remaining battery time
     * 
     * @return the remaining battery time
     */
    public double getBatteryTimeRemaining() {
        double remaining = 0;
        if (electromotor.getCurrentPower() > 1) {
            remaining = (battery.getCurrentCapacity() * 3600)
                    / (electromotor.getCurrentPower());
        } else
            remaining = Double.POSITIVE_INFINITY;
        return remaining;
    }

    /**
     * Checks the auxiliary battery time
     * 
     * @return
     * @throws BatteryCapacityUnterAuxiliaryException
     */
    private boolean checkBatteryTimeRemaining()
            throws BatteryCapacityUnterAuxiliaryException {
        if (getCurrentFlightphase().getCurrentPowerConsumption() > 0) {
            double remaining = (battery.getCurrentCapacity() * 3600)
                    / getCurrentFlightphase().getCurrentPowerConsumption();
            if (remaining < MIN_AUXILIARY_BATTERY_TIME)
                throw new BatteryCapacityUnterAuxiliaryException();
            return remaining < MIN_AUXILIARY_BATTERY_TIME;
        }
        return true;

    }

    /**
     * Returns the distance of the flight
     * 
     * @return The distance
     */
    public double getDistance() {
        double distance = 0;
        for (Flightphase flightphase : this.flight.getFlightphases()) {
            distance += flightphase.getHorizontalDistance() / 1000;
        }
        return distance;
    }

    /**
     * Returns the power consumption of the flight
     * 
     * @return The power consumption
     */
    public double getPowerConsumption() {
        double powerConsumption = 0;
        for (Flightphase flightphase : this.flight.getFlightphases()) {
            powerConsumption += flightphase.getPowerConsumption();
        }
        return powerConsumption;
    }

    /**
     * Returns the time of the flight
     * 
     * @return The time
     */
    public double getFlightTime() {
        double time = 0;
        for (Flightphase flightphase : this.flight.getFlightphases()) {
            time += flightphase.getFlightTime();
        }
        return time;
    }

    /**
     * Returns the planned battery capacity
     * 
     * @return The planned battery capacity
     */
    public List<Double> getPlannedBatteryCapacity() {
        return plannedBatteryCapacity;
    }

    /**
     * Returns the flight
     * 
     * @return The flight
     */
    public Flight getFlight() {
        return this.flight;
    }
}
