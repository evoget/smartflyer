package ch.bfh.smartflyer.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.bfh.smartflyer.domain.Battery;
import ch.bfh.smartflyer.domain.Electromotor;
import ch.bfh.smartflyer.domain.Flight;
import ch.bfh.smartflyer.domain.FlightPhaseType;
import ch.bfh.smartflyer.domain.Flightphase;
import ch.bfh.smartflyer.domain.RangeExtender;
import ch.bfh.smartflyer.domain.Smartflyer;
import ch.bfh.smartflyer.domain.Tank;

/**
 * A simulator for the Smartflyer
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class Simulator implements Smartflyer {

    /**
     * logger for simulator class
     */
    private static final Logger LOGGER = Logger.getLogger(Simulator.class);

    private static final int MIN_RANGE_EXTENDER_ALTITUDE = 600;
    private static final double MIN_AUXILIARY_BATTERY_TIME = 5.0 * 60.0; // 10min
    private static final double BATTERY_LOAD_CAPACITY = 97;

    private Flight flight;
    private Electromotor electromotor;
    private RangeExtender rangeExtender;
    private Battery battery;
    private Tank tank;
    private List<Double> realBatteryCapacity = new ArrayList<>();
    private int flightDuration;
    private int flyingSeconds = 0;
    private double currentAltitude = 0;
    private boolean automaticRangeExtender = true;
    private double timeAcceleration = 1;
    private boolean flying = false;
    private boolean simulationFinished = false;
    private double secondsHelper = 0;

    /**
     * Creates a new Smartflyer simulator
     * 
     * @param flight
     *            The flight
     * @param electromotor
     *            The electromotor
     * @param rangeExtender
     *            The range extender
     * @param battery
     *            The battery
     * @param tank
     *            The tank
     */
    public Simulator(Flight flight, RangeExtender rangeExtender,
            Battery battery, Tank tank) {
        super();
        this.electromotor = new Electromotor(160, 0);
        this.rangeExtender = rangeExtender;
        this.rangeExtender.disableRangeExtender();
        this.battery = battery;
        this.tank = tank;
        this.flight = flight;
        this.flightDuration = getFlightDuration();
    }

    /**
     * Calculates the duration of the flight
     * 
     * @return
     */
    @Override
    public int getFlightDuration() {
        double time = 0;
        for (Flightphase flightphase : this.flight.getFlightphases()) {
            time += flightphase.getFlightTime();
        }
        return (int) time;
    }

    /**
     * Starts the simulation
     */
    @Override
    public void startSimulation() {
        if (!simulationFinished) {
            flying = true;
            new Thread() {
                @Override
                public void run() {
                    while (flying) {
                        if (flyingSeconds < flightDuration) {
                            flyingSeconds++;
                            currentAltitude += getCurrentFlightphase()
                                    .getRateOfClimb();
                            checkRangeExtender();
                            electromotor
                                    .setCurrentPower(getCurrentFlightphase()
                                            .getCurrentPowerConsumption());
                            battery.setCurrentCapacity((battery
                                    .getCurrentCapacity() * 1000 * 3600 - electromotor
                                    .getCurrentPower() * 1000)
                                    / (1000 * 3600));
                            if (rangeExtender.getEnabled()) {
                                tank.setCurrentCapacity(tank
                                        .getCurrentCapacity()
                                        - rangeExtender.getPetrolConsumption());
                                battery.setCurrentCapacity(battery
                                        .getCurrentCapacity()
                                        + (rangeExtender.getPower() / 3600));
                            }
                            realBatteryCapacity.add(battery
                                    .getCurrentCapacity());
                        } else {
                            flying = false;
                            simulationFinished = true;
                        }
                        try {
                            Thread.sleep((long) (1000 / timeAcceleration));
                        } catch (InterruptedException e) {
                            LOGGER.error("Could not simulate flight", e);
                        }
                    }
                }
            }.start();
        }
    }

    /**
     * Stops the simulation
     */
    @Override
    public void stopSimulation() {
        flying = false;
    }

    /**
     * Returns the true air speed
     * 
     * @return The true air speed
     */
    @Override
    public double getCurrentSpeed() {
        return getCurrentFlightphase().getSpeed();
    }

    /**
     * Returns the current flightphase
     * 
     * @return The current flightphase
     */
    @Override
    public Flightphase getCurrentFlightphase() {
        List<Flightphase> phases = flight.getFlightphases();
        Flightphase currentPhase = phases.get(0);
        secondsHelper = 0;

        for (Flightphase flightphase : phases) {
            secondsHelper += flightphase.getFlightTime();
            if (flyingSeconds < secondsHelper) {
                currentPhase = flightphase;
                break;
            }
        }
        return currentPhase;
    }

    /**
     * Return the time remaining in the current flightphase
     * 
     * @return
     */
    @Override
    public int getFlightphaseTimeRemaining() {
        getCurrentFlightphase();
        return (int) (secondsHelper - flyingSeconds);
    }

    /**
     * Checks if the range Extender should be enabled
     */
    private void checkRangeExtender() {
        if (automaticRangeExtender) {
            if (currentAltitude >= MIN_RANGE_EXTENDER_ALTITUDE
                    || getCurrentFlightphase().getFlightPhaseType() == FlightPhaseType.AIRPORT_STOP
                    || getCurrentFlightphase().getFlightPhaseType() == FlightPhaseType.AIRPORT_STOP_POWERSOURCE) {
                if (Double.compare(battery.getCurrentCapacity(),
                        battery.getMaxCapacity()) == 0) {
                    rangeExtender.disableRangeExtender();
                }
                if ((100 / battery.getMaxCapacity() * battery
                        .getCurrentCapacity()) < BATTERY_LOAD_CAPACITY) {
                    rangeExtender.enableRangeExtender();
                }
            } else {
                if (getBatteryTimeRemaining() < MIN_AUXILIARY_BATTERY_TIME
                        && Double.compare(battery.getCurrentCapacity(),
                                battery.getMaxCapacity()) != 0) {
                    rangeExtender.enableRangeExtender();
                } else
                    rangeExtender.disableRangeExtender();
            }
        }
    }

    /**
     * Returns the remaining flight time
     * 
     * @return
     */
    @Override
    public double getFlightTimeRemaining() {
        return (double) flightDuration - flyingSeconds;

    }

    /**
     * Returns the current altitude
     * 
     * @return The current altitude
     */
    @Override
    public double getCurrentAltitude() {
        return currentAltitude;
    }

    /**
     * Return the remaining tank time
     * 
     * @return
     */
    @Override
    public double getTankTimeRemaining() {
        if (rangeExtender.getEnabled())
            return tank.getCurrentCapacity()
                    / rangeExtender.getPetrolConsumption();
        else
            return tank.getMaxCapacity() / rangeExtender.getPetrolConsumption();
    }

    /**
     * Set the time acceleration for the simulation
     * 
     * @param timeAcceleration
     *            The timeAcceleration
     */
    @Override
    public void setTimeAcceleration(int timeAcceleration) {
        this.timeAcceleration = timeAcceleration;
    }

    /**
     * Returns the battery time remaining
     * 
     * @return The battery time remaining
     */
    @Override
    public double getBatteryTimeRemaining() {
        double remaining = 0;
        if (electromotor.getCurrentPower() > 1) {
            remaining = (battery.getCurrentCapacity() * 3600)
                    / (electromotor.getCurrentPower());
        } else
            remaining = Double.POSITIVE_INFINITY;
        return remaining;
    }

    /**
     * Return the remaining distance with the current battery load
     */
    @Override
    public double getBatteryDistanceRemaining() {
        if (Double.compare(getBatteryTimeRemaining(), Double.POSITIVE_INFINITY) == 0) {
            return Double.POSITIVE_INFINITY;
        }
        return getBatteryTimeRemaining() * getCurrentSpeed();
    }

    /**
     * Returns the ambient temperature
     * 
     * @return The ambient temperature
     */
    @Override
    public double getAmbientTemperature() {
        return 20 - (getCurrentAltitude() * 0.006);
    }

    /**
     * Enables the range extender
     */
    @Override
    public void enableRangeExtender() {
        automaticRangeExtender = false;
        this.rangeExtender.enableRangeExtender();
    }

    /**
     * Disables the range extender
     */
    @Override
    public void disableRangeExtender() {
        automaticRangeExtender = false;
        this.rangeExtender.disableRangeExtender();
    }

    /**
     * Set the range extender in the automatic mode
     */
    @Override
    public void setAutomaticRangeExtender() {
        automaticRangeExtender = true;
        checkRangeExtender();
    }

    /**
     * Return the maximum electromotor power
     * 
     * @return The maximum electromotor power
     */
    @Override
    public double getMaxElectromotorPower() {
        return electromotor.getMaxPower();
    }

    /**
     * Return the current electromotor power
     * 
     * @return The current electromotor power
     */
    @Override
    public double getCurrentElectromotorPower() {
        return electromotor.getCurrentPower();
    }

    /**
     * Returns the range extender power
     * 
     * @return The range extender power
     */
    @Override
    public double getRangeExtenderPower() {
        return rangeExtender.getPower();
    }

    /**
     * Returns the maximum battery capacity
     * 
     * @return The range extender power
     */
    @Override
    public double getMaxBatteryCapacity() {
        return battery.getMaxCapacity();
    }

    /**
     * Returns the current battery capacity
     * 
     * @return The current battery capacity
     */
    @Override
    public double getCurrentBatteryCapacity() {
        return battery.getCurrentCapacity();
    }

    /**
     * Returns the max tank capacity
     * 
     * @return The max tank capacity
     */
    @Override
    public double getMaxTankCapacity() {
        return tank.getMaxCapacity();
    }

    /**
     * Returns the current tank capacity
     * 
     * @return The current tank capacity
     */
    @Override
    public double getCurrentTankCapacity() {
        return tank.getCurrentCapacity();
    }

    /**
     * Returns if the range extender is enabled
     * 
     * @return If the range extender is enabled
     */
    @Override
    public boolean getRangeExtenderEnabled() {
        return rangeExtender.getEnabled();
    }

    @Override
    public boolean getAutomaticRangeExtender() {
        return automaticRangeExtender;
    }

    @Override
    public boolean isStarted() {
        return flying;
    }

    @Override
    public int flightTimeRemaining() {
        return flightDuration - flyingSeconds;
    }

    @Override
    public List<Double> getRealBatteryCapacity() {
        return realBatteryCapacity;

    }

    @Override
    public int getFlyingSeconds() {
        return flyingSeconds;
    }

}
