package ch.bfh.smartflyer.persistence;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.Pilot;

public class DBQueryImplTest {

	private TestAppender appender;

	@Before
	public void initializeLogger() {

		// used to check if errors were logged correctly
		appender = new TestAppender();
		final Logger logger = Logger.getRootLogger();
		logger.addAppender(appender);

		Logger.getLogger(DBQueryImpl.class.getName());
	}

	@Test
	public void testGetPilotByIdNotFound() throws SQLException {

		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		// Prepare Query
		String sql = "SELECT * FROM `pilot`, `airport` WHERE pilot.PID = ? AND pilot.FavouriteAirportID = airport.PID";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		pst.setInt(1, 10);

		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(false);

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		Pilot res = q.getPilotById(10);
		assertEquals(null, res);

		// Verification
		control.verify();

		// get log message
		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals(
				"No Pilot with the id 10 was found in the database",
				firstLogEntry.getMessage().toString());
	}

	@Test
	public void testGetPilotById() throws SQLException {

		IMocksControl control = EasyMock.createControl();

		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		String sql = "SELECT * FROM `pilot`, `airport` WHERE pilot.PID = ? AND pilot.FavouriteAirportID = airport.PID";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		pst.setInt(1, 10);

		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(true);
		EasyMock.expect(rs.getInt("airport.PID")).andReturn(1);
		EasyMock.expect(rs.getString("City")).andReturn("Bern");
		EasyMock.expect(rs.getString("IATA")).andReturn("BB");
		EasyMock.expect(rs.getString("ICAO")).andReturn("BB");
		EasyMock.expect(rs.getDouble("Latitude")).andReturn(2.1);
		EasyMock.expect(rs.getDouble("Longitude")).andReturn(1.2);

		EasyMock.expect(rs.getInt("pilot.PID")).andReturn(10);
		EasyMock.expect(rs.getString("Firstname")).andReturn("Hans");
		EasyMock.expect(rs.getString("Lastname")).andReturn("Muster");
		EasyMock.expect(rs.getDouble("Weight")).andReturn(78.5);

		pst.close();

		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		Pilot res = q.getPilotById(10);

		Airport airport = new Airport(1, "Bern", "BB", "BB", 2.1, 1.2);
		Pilot expected = new Pilot(10, "Hans", "Muster", 78.5, airport);

		assertEquals(expected, res);

		control.verify();
	}

	@Test
	public void testInsertFlight() throws SQLException {

		IMocksControl control = EasyMock.createControl();

		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		String sql = "INSERT INTO `flight` (`PilotID`, `FromAirportID`, `ToAirportID`, `Date`) "
				+ "VALUES (?,?,?,?)";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		Timestamp t = new Timestamp(new java.util.Date().getTime());

		pst.setInt(1, 20);
		pst.setInt(2, 13);
		pst.setInt(3, 14);
		pst.setTimestamp(4, t);

		EasyMock.expect(pst.executeUpdate()).andReturn(1);

		pst.close();

		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		q.insertFlight(20, 13, 14, t);

		control.verify();
	}

	@Test
	public void testInsertFlightSQLException() throws SQLException, IOException {

		IMocksControl control = EasyMock.createControl();

		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		String sql = "INSERT INTO `flight` (`PilotID`, `FromAirportID`, `ToAirportID`, `Date`) "
				+ "VALUES (?,?,?,?)";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		Timestamp t = new Timestamp(new java.util.Date().getTime());

		pst.setInt(1, 20);
		pst.setInt(2, 13);
		pst.setInt(3, 14);
		pst.setTimestamp(4, t);

		EasyMock.expect(pst.executeUpdate()).andThrow(new SQLException());

		pst.close();

		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		q.insertFlight(20, 13, 14, t);

		control.verify();

		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals("Could not Execute Query \"addFlight\"",
				firstLogEntry.getMessage().toString());
	}

	@Test
	public void testInsertPilot() throws SQLException {

		IMocksControl control = EasyMock.createControl();

		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		String sql = "INSERT INTO `pilot` (`Firstname`, `Lastname`, `Weight`, `FavouriteAirportID`) "
				+ "VALUES (?,?,?,?)";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		Airport a = new Airport(1, "test", "test", "test", 23.0, 22.0);

		Pilot p = new Pilot(1, "test", "test", 76.0, a);

		pst.setString(1, p.getFirstname());
		pst.setString(2, p.getLastname());
		pst.setDouble(3, p.getWeight());
		pst.setInt(4, p.getFavouriteAirport().getId());

		EasyMock.expect(pst.executeUpdate()).andReturn(1);

		pst.close();

		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		q.insertPilot(p);

		control.verify();
	}

	@Test
	public void testInsertPilotSQLException() throws SQLException {

		IMocksControl control = EasyMock.createControl();

		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		String sql = "INSERT INTO `pilot` (`Firstname`, `Lastname`, `Weight`, `FavouriteAirportID`) "
				+ "VALUES (?,?,?,?)";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		Airport a = new Airport(1, "test", "test", "test", 23.0, 22.0);

		Pilot p = new Pilot(1, "test", "test", 76.0, a);

		pst.setString(1, p.getFirstname());
		pst.setString(2, p.getLastname());
		pst.setDouble(3, p.getWeight());
		pst.setInt(4, p.getFavouriteAirport().getId());

		EasyMock.expect(pst.executeUpdate()).andThrow(new SQLException());

		pst.close();

		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		q.insertPilot(p);

		control.verify();

		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals("Could not Execute Query \"insertPilot\"",
				firstLogEntry.getMessage().toString());
	}

	@Test
	public void testGetAllPilotsNoFound() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		// Prepare Query
		String sql = "SELECT * FROM `pilot`, `airport` WHERE pilot.FavouriteAirportID = airport.PID";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(false);

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Pilot> pilots = q.getAllPilots();
		Assert.assertTrue(pilots.size() == 0);

		// Verification
		control.verify();

		// get log message
		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals(
				"No Pilots were found in database. Please ensure, "
						+ "that you inserted demo pilot data into database.",
				firstLogEntry.getMessage().toString());
	}
	
	@Test
	public void testGetAllPilots() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		// Prepare Query
		String sql = "SELECT * FROM `pilot`, `airport` WHERE pilot.FavouriteAirportID = airport.PID";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);
		
		Airport a = new Airport(1, "Bern", "BB", "BB", 23.0, 22.0);
		
		List<Pilot> pilots = new ArrayList<Pilot>();
		pilots.add(new Pilot(10, "Hans", "Muster", 78.5, a));
		
		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(true);
		EasyMock.expect(rs.next()).andReturn(false);
		
		EasyMock.expect(rs.getInt("airport.PID")).andReturn(1);
		EasyMock.expect(rs.getString("City")).andReturn("Bern");
		EasyMock.expect(rs.getString("IATA")).andReturn("BB");
		EasyMock.expect(rs.getString("ICAO")).andReturn("BB");
		EasyMock.expect(rs.getDouble("Latitude")).andReturn(2.1);
		EasyMock.expect(rs.getDouble("Longitude")).andReturn(1.2);

		EasyMock.expect(rs.getInt("pilot.PID")).andReturn(10);
		EasyMock.expect(rs.getString("Firstname")).andReturn("Hans");
		EasyMock.expect(rs.getString("Lastname")).andReturn("Muster");
		EasyMock.expect(rs.getDouble("Weight")).andReturn(78.5);

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Pilot> pilotsResult = q.getAllPilots();
		Assert.assertTrue(pilotsResult.size() == 1);

		// Verification
		control.verify();
	}
	
	@Test
	public void testGetAllPilotsSQLException() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		// Prepare Query
		String sql = "SELECT * FROM `pilot`, `airport` WHERE pilot.FavouriteAirportID = airport.PID";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andThrow(new SQLException());

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Pilot> pilots = q.getAllPilots();
		Assert.assertTrue(pilots.size() == 0);

		// Verification
		control.verify();

		// get log message
		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals(
				"Could not Execute Query \"getAllPilots\"",
				firstLogEntry.getMessage().toString());
	}
	
	@Test
	public void testGetAllAirportsNoFound() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		// Prepare Query
		String sql = "SELECT * FROM `airport`";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(false);

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Airport> airports = q.getAllAirports();
		Assert.assertTrue(airports.size() == 0);

		// Verification
		control.verify();

		// get log message
		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals(
				"No Airports were found in database. Please ensure, "
						+ "that you inserted demo airport data into database.",
				firstLogEntry.getMessage().toString());
	}
	
	@Test
	public void testGetAllAirports() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);
		ResultSet rs = control.createMock(ResultSet.class);

		// Prepare Query
		String sql = "SELECT * FROM `airport`";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);
		
		List<Airport> airports = new ArrayList<Airport>();
		airports.add(new Airport(1, "Bern", "BB", "BB", 12.0, 11.0));
		
		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andReturn(rs);
		EasyMock.expect(rs.next()).andReturn(true);
		EasyMock.expect(rs.next()).andReturn(false);
		
		EasyMock.expect(rs.getInt("airport.PID")).andReturn(1);
		EasyMock.expect(rs.getString("City")).andReturn("Bern");
		EasyMock.expect(rs.getString("IATA")).andReturn("BB");
		EasyMock.expect(rs.getString("ICAO")).andReturn("BB");
		EasyMock.expect(rs.getDouble("Latitude")).andReturn(12.0);
		EasyMock.expect(rs.getDouble("Longitude")).andReturn(11.0);

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Airport> airportsResult = q.getAllAirports();
		Assert.assertTrue(airportsResult.size() == 1);

		// Verification
		control.verify();
	}
	
	@Test
	public void testGetAllAirportsSQLException() throws SQLException {
		IMocksControl control = EasyMock.createControl();

		// Create Mock classes
		Connection conn = control.createMock(Connection.class);
		PreparedStatement pst = control.createMock(PreparedStatement.class);

		// Prepare Query
		String sql = "SELECT * FROM `airport`";
		EasyMock.expect(conn.prepareStatement(sql)).andReturn(pst).times(1);

		// Expected method calls
		EasyMock.expect(pst.executeQuery()).andThrow(new SQLException());

		pst.close();

		// Recording is completed, switch the replay state
		control.replay();

		DBQueryImpl q = new DBQueryImpl(conn);

		// The actual method is invoked
		List<Airport> airports = q.getAllAirports();
		Assert.assertTrue(airports.size() == 0);

		// Verification
		control.verify();

		// get log message
		List<LoggingEvent> log = appender.getLog();
		LoggingEvent firstLogEntry = log.get(0);

		Assert.assertEquals(
				"Could not Execute Query \"getAllAirports\"",
				firstLogEntry.getMessage().toString());
	}

	// used to get log messages from class
	class TestAppender extends AppenderSkeleton {
		private final List<LoggingEvent> log = new ArrayList<LoggingEvent>();

		@Override
		public boolean requiresLayout() {
			return false;
		}

		@Override
		protected void append(final LoggingEvent loggingEvent) {
			log.add(loggingEvent);
		}

		@Override
		public void close() {
		}

		public List<LoggingEvent> getLog() {
			return new ArrayList<LoggingEvent>(log);
		}
	}
}
