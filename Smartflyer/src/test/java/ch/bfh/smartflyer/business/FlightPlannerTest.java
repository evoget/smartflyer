/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.business;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.AirportVia;
import ch.bfh.smartflyer.domain.Battery;
import ch.bfh.smartflyer.domain.Flight;
import ch.bfh.smartflyer.domain.Pilot;
import ch.bfh.smartflyer.domain.RangeExtender;
import ch.bfh.smartflyer.domain.Tank;
import ch.bfh.smartflyer.support.BatteryCapacityUnterAuxiliaryException;
import ch.bfh.smartflyer.support.FlightTooShortException;
import ch.bfh.smartflyer.support.NotEnoughPetrolException;

/**
 * JUnit Test for FlightPlanner class
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class FlightPlannerTest {

    private Flight flight;
    private RangeExtender extender;
    private Battery battery;
    private Tank tank;
    
    @Before
    public void setUp(){
        Airport start = new Airport(1639, "GENEVA COINTRIN", "GVA", "LSGG", 46.2380555555, 6.1088888888);
        Airport stop = new Airport(1644, "ST GALLEN ALTENRHEIN", "ACH", "LSZR", 47.4849999999, 9.5605555555);
        
        Pilot p = new Pilot(12, "Hans", "Muster", 75.2, stop);
        
        this.flight = new Flight(start, stop, new ArrayList<AirportVia>(), p, 6000.0);
        this.extender = new RangeExtender(70.0, 1.0);
        this.battery = new Battery(22.5, 22.5);
        this.tank = new Tank(50.0, 50.0);
    }
    
    
    @Test(expected=FlightTooShortException.class)
    public void testCreateFlightToShort() throws FlightTooShortException, NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException{
        
        this.flight.setAltitude(9000.0);
        
        FlightPlanner planner = new FlightPlanner(flight, extender, battery, tank);
    }
    
    @Test(expected=NotEnoughPetrolException.class)
    public void testCreateFlightNotEnoughPetrol() throws FlightTooShortException, NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException{
        
        this.tank.setCurrentCapacity(0.0);
        
        FlightPlanner planner = new FlightPlanner(flight, extender, battery, tank);
    }
    
    @Test(expected=BatteryCapacityUnterAuxiliaryException.class)
    public void testCreateFlightNotEnoughBattery() throws FlightTooShortException, NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException{
        
        this.battery.setCurrentCapacity(0.0);
        
        FlightPlanner planner = new FlightPlanner(flight, extender, battery, tank);
    }
    
    @Test
    public void testCreateFlight() throws FlightTooShortException, NotEnoughPetrolException, BatteryCapacityUnterAuxiliaryException{
        
        FlightPlanner planner = new FlightPlanner(flight, extender, battery, tank);
        
        Assert.assertEquals(5707.5, planner.getFlightTime(), 1.0);
        Assert.assertTrue(!planner.getPlannedBatteryCapacity().isEmpty());
        Assert.assertTrue(planner.getDistance() > 0.0);
        Assert.assertTrue(planner.getPowerConsumption() > 0.0);
        Assert.assertNotNull(planner.getFlight());
    }
}
