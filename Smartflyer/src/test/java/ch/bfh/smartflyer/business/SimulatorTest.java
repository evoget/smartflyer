/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.business;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.bfh.smartflyer.domain.Airport;
import ch.bfh.smartflyer.domain.AirportVia;
import ch.bfh.smartflyer.domain.Battery;
import ch.bfh.smartflyer.domain.Flight;
import ch.bfh.smartflyer.domain.Pilot;
import ch.bfh.smartflyer.domain.RangeExtender;
import ch.bfh.smartflyer.domain.StartPhase;
import ch.bfh.smartflyer.domain.Tank;

/**
 * JUnit Test for FlightPlanner class
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class SimulatorTest {

    private Flight flight;
    private RangeExtender extender;
    private Battery battery;
    private Tank tank;
    
    @Before
    public void setUp(){
        Airport start = new Airport(1639, "GENEVA COINTRIN", "GVA", "LSGG", 46.2380555555, 6.1088888888);
        Airport stop = new Airport(1644, "ST GALLEN ALTENRHEIN", "ACH", "LSZR", 47.4849999999, 9.5605555555);
        
        Pilot p = new Pilot(12, "Hans", "Muster", 75.2, stop);
        
        this.flight = new Flight(start, stop, new ArrayList<AirportVia>(), p, 6000.0);
        this.extender = new RangeExtender(70.0, 1.0);
        this.battery = new Battery(22.5, 22.5);
        this.tank = new Tank(50.0, 50.0);
    }
    
    
    @Test
    public void testGetFlightDuration() {

        Simulator s = new Simulator(flight, extender, battery, tank);
        
        Assert.assertEquals(5707, s.getFlightDuration());
        Assert.assertFalse(s.isStarted());
    }
    
    @Test
    public void testCreateSimulation() {
        
        Simulator s = new Simulator(flight, extender, battery, tank);
        s.setTimeAcceleration(1);
        
        System.out.println(s.isStarted());
        
        Assert.assertTrue(s.getAutomaticRangeExtender());
        Assert.assertEquals(22.5, s.getCurrentBatteryCapacity(), 0.0);
        Assert.assertEquals(0.0, s.getCurrentElectromotorPower(), 0.0);
        Assert.assertTrue(s.getCurrentSpeed() > 0.0);
        Assert.assertTrue(s.getCurrentTankCapacity() > 0.0);
        Assert.assertTrue(s.getFlightDuration() > 0);
        Assert.assertTrue(s.getFlightphaseTimeRemaining() > 0);
        Assert.assertEquals(0, s.getFlyingSeconds());
        Assert.assertTrue(s.getTankTimeRemaining() > 0.0);
        Assert.assertEquals(StartPhase.class, s.getCurrentFlightphase().getClass());
    }
    
    
}
