/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.support;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit Test for AirportDistanceCalculator class
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class AirportDistanceCalculatorTest {

	@Test
	public void testCalculateDistance(){
		
		double lat1 = 47.4647222222;
		double lng1 = 8.5491666666;
		double lat2 = 46.9138888888;
		double lng2 = 7.4969444444;
		
		double dist = AirportDistanceCalculator.calculateDistance(lat1, lng1, lat2, lng2);
		
		Assert.assertEquals(100366.1, dist, 0.3);
	}
	
}
