/*
 * Copyright (c) 2015 by Thomas Vogel and Philip Vandermeulen
 * Bern University of Applied Sciences - Engineering and Information Technology
 *
 * Project Smartflyer
 */
package ch.bfh.smartflyer.domain;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit Test for Airport Class.
 * 
 * @author Thomas Vogel, Philip Vandermeulen
 *
 */
public class AirportTest {

	@Test
	public void testEquals(){
		
		Airport a1 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		Airport a2 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		
		Assert.assertTrue(a1.equals(a2));
		Assert.assertTrue(a1.equals(a1));
		Assert.assertEquals(a1.hashCode(), a2.hashCode());
		
		Assert.assertEquals(a1.getId(), a2.getId());
		Assert.assertEquals(a1.getIata(), a2.getIata());
		Assert.assertEquals(a1.getIcao(), a2.getIcao());
		Assert.assertEquals(a1.getName(), a2.getName());
		Assert.assertEquals(a1.getLatitude(), a2.getLatitude(), 0.1);
		Assert.assertEquals(a1.getLongitude(), a2.getLongitude(), 0.1);
	}
	
	@Test
	public void testEqualsNull(){
		
		Airport a1 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		
		Assert.assertFalse(a1.equals(null));
	}
	
	@Test
	public void testEqualsWrongClass(){
		
		Airport a1 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		Battery b = new Battery(1.0, 2.0);
		
		Assert.assertFalse(a1.equals(b));
	}
	
	@Test
	public void testEqualsICAONull(){
		
		Airport a1 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		Airport a2 = new Airport(1, "Bern Belp", "BBB", null, 23.0, 24.0);
		
		Assert.assertFalse(a1.equals(a2));
		Assert.assertFalse(a2.equals(a1));
	}
	
	@Test
	public void testEqualsNotSame(){
		
		Airport a1 = new Airport(1, "Bern Belp", "BBB", "BBBB", 23.0, 24.0);
		Airport a2 = new Airport(1, "Basel", "BAL", "BASL", 25.0, 26.0);
		
		Assert.assertFalse(a1.equals(a2));
	}
	
}
